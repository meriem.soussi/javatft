package admin.interfaces.gamer;

import interfaces.BgBorder;
import interfaces.Login;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import entities.Utilisateur;

@SuppressWarnings("serial")
public class GamerShowDialog extends JDialog implements ActionListener {
	
	JButton btn_retour;
	
	
	
	public GamerShowDialog(Utilisateur u){
		this.setTitle(u.getNom()+" "+u.getPrenom());
		this.setResizable(false);
		this.setBounds(100, 100, 800, 445);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(null);
		this.setModalityType(DEFAULT_MODALITY_TYPE);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 800, 445);
		this.getContentPane().add(panel);
		
		
		
		try {
		     BgBorder borde = new BgBorder(ImageIO.read(Login.class.getResource("/images/tennis05.jpeg")) );            
		     (panel).setBorder(borde);            
		     panel.setLayout(null);
		} catch (IOException e) {e.printStackTrace();}
		/////
		
		JLabel lblNom = new JLabel("Nom: "+u.getNom());
	    lblNom.setHorizontalAlignment(SwingConstants.LEFT);
	    lblNom.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblNom.setForeground(new Color(238, 203, 51));
	    lblNom.setBackground(Color.BLACK);
	    lblNom.setBounds(50, 50, 400, 22);
	    panel.add(lblNom);
		//
	    JLabel lblPrenom = new JLabel("Prenom: "+u.getPrenom());
	    lblPrenom.setHorizontalAlignment(SwingConstants.LEFT);
	    lblPrenom.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblPrenom.setForeground(new Color(238, 203, 51));
	    lblPrenom.setBackground(Color.BLACK);
	    lblPrenom.setBounds(50, 80, 400, 22);
	    panel.add(lblPrenom);
	    //
	    JLabel lblCin = new JLabel("N�CIN: "+u.getCin());
	    lblCin.setHorizontalAlignment(SwingConstants.LEFT);
	    lblCin.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblCin.setForeground(new Color(238, 203, 51));
	    lblCin.setBackground(Color.BLACK);
	    lblCin.setBounds(50, 110, 400, 22);
	    panel.add(lblCin);
	    //
	    JLabel lblAdresse = new JLabel("Adresse: "+u.getAdresse());
	    lblAdresse.setHorizontalAlignment(SwingConstants.LEFT);
	    lblAdresse.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblAdresse.setForeground(new Color(238, 203, 51));
	    lblAdresse.setBackground(Color.BLACK);
	    lblAdresse.setBounds(50, 140, 400, 22);
	    panel.add(lblAdresse);
	    //
	    JLabel lblTelephone = new JLabel("T�l�phone: "+u.getCin());
	    lblTelephone.setHorizontalAlignment(SwingConstants.LEFT);
	    lblTelephone.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblTelephone.setForeground(new Color(238, 203, 51));
	    lblTelephone.setBackground(Color.BLACK);
	    lblTelephone.setBounds(50, 170, 400, 22);
	    panel.add(lblTelephone);
	    //
	    JLabel lblGrade = new JLabel("Grade: "+u.getGrade());
	    lblGrade.setHorizontalAlignment(SwingConstants.LEFT);
	    lblGrade.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblGrade.setForeground(new Color(238, 203, 51));
	    lblGrade.setBackground(Color.BLACK);
	    lblGrade.setBounds(50, 200, 400, 22);
	    panel.add(lblGrade);
	    //
	    JLabel lblEmail = new JLabel("E-mail: "+u.getEmail());
	    lblEmail.setHorizontalAlignment(SwingConstants.LEFT);
	    lblEmail.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblEmail.setForeground(new Color(238, 203, 51));
	    lblEmail.setBackground(Color.BLACK);
	    lblEmail.setBounds(50, 230, 400, 22);
	    panel.add(lblEmail);
	    //
	    JLabel lblRole = new JLabel("Role: "+u.getRoles());
	    lblRole.setHorizontalAlignment(SwingConstants.LEFT);
	    lblRole.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblRole.setForeground(new Color(238, 203, 51));
	    lblRole.setBackground(Color.BLACK);
	    lblRole.setBounds(50, 260, 400, 22);
	    panel.add(lblRole);
	     
	    
	    ImageIcon img = null;
		try {
			img = new ImageIcon(ImageIO.read(new File("C:/imagesUtilisateurs/"+u.getCin()+".jpg")));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    if(img!=null){
	    	JLabel imagelabel = new JLabel(img);
		    imagelabel.setBounds(450, 45, 300, 300);
		    panel.add(imagelabel);
	    }
	    else{
	    	JLabel imagelabel = new JLabel("Pas d'image");
		    imagelabel.setBounds(450, 45, 300, 300);
		    panel.add(imagelabel);
	    }
	   
	    
		
			//////
			btn_retour = new JButton("Retour");
			btn_retour.addActionListener(this);
			btn_retour.setForeground(Color.BLUE);
			btn_retour.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
			btn_retour.setBounds(300, 353, 200, 29);
		    panel.add(btn_retour);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btn_retour) {this.dispose();}

	}
	
}
