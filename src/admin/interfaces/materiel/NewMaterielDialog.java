package admin.interfaces.materiel;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import dao.DAOMateriel;
import dao.DAOUtilisateur;
import entities.Materiel;
import entities.Utilisateur;
import interfaces.BgBorder;
import interfaces.Login;
import mail.REmailUser;

public class NewMaterielDialog extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	JTextField textField_nom;
	JComboBox<Integer> select_quantite;
	JComboBox<String> select_user;
	
	DAOMateriel daom;
	Materiel m;
	
	DAOUtilisateur daou = new DAOUtilisateur();
	List<Utilisateur> listUtilisateur = new ArrayList<Utilisateur>();
	
	MaterielDialog parent;
	
	JButton btn_retour;
	JButton btn_appliquer;
	
	public NewMaterielDialog(DAOMateriel dao, MaterielDialog par){
		
		this.daom = dao;
		m = new Materiel();
		this.parent = par;
		
		
		
		this.setTitle("Nouveau Materiel");
		this.setResizable(false);
		this.setBounds(100, 100, 800, 445);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(null);
		this.setModalityType(DEFAULT_MODALITY_TYPE);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 800, 445);
		this.getContentPane().add(panel);
		
		
		
		try {
		     BgBorder borde = new BgBorder(ImageIO.read(Login.class.getResource("/images/tennis05.jpeg")) );            
		     (panel).setBorder(borde);            
		     panel.setLayout(null);
		} catch (IOException e) {e.printStackTrace();}
		/////
		
		// nom
		JLabel lbNom = new JLabel("Nom: ");
		lbNom.setHorizontalAlignment(SwingConstants.RIGHT);
		lbNom.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
		lbNom.setForeground(new Color(238, 203, 51));
		lbNom.setBackground(Color.BLACK);
		lbNom.setBounds(99, 50, 200, 22);
	    panel.add(lbNom);
	    textField_nom = new JTextField();
	    
	    textField_nom.setBounds(300, 50, 200, 29);
	    panel.add(textField_nom);
	   
	    
	    // 50/110/140/170/200
	    //  Quantite
	    JLabel lblQuantite = new JLabel("Quantit�: ");
	    lblQuantite.setHorizontalAlignment(SwingConstants.RIGHT);
	    lblQuantite.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblQuantite.setForeground(new Color(238, 203, 51));
	    lblQuantite.setBackground(Color.BLACK);
	    lblQuantite.setBounds(99, 82, 200, 22);
	    panel.add(lblQuantite);
	    Integer[] quantite = {1,2,3,4,5,6,7,8,9,10};
		select_quantite = new JComboBox<Integer>(quantite);
		select_quantite.setBounds(300, 82, 200, 29);
	    panel.add(select_quantite);
	    
	    /////////////
	    // User
	    JLabel lblUser = new JLabel("Utilisateur: ");
	    lblUser.setHorizontalAlignment(SwingConstants.RIGHT);
	    lblUser.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblUser.setForeground(new Color(238, 203, 51));
	    lblUser.setBackground(Color.BLACK);
	    lblUser.setBounds(99, 114, 200, 22);
	    panel.add(lblUser);
		
	    List<Utilisateur> listGamers = new ArrayList<Utilisateur>();
	    listGamers = daou.getGamers();
	    List<Utilisateur> listArbitres = new ArrayList<Utilisateur>();
	    listArbitres = daou.getArbitres();
	    
	     int tg = listGamers.size();
	     for(int i=0;i<tg;i++){
	    	 listUtilisateur.add(listGamers.get(i));
	     }
	     int ta = listArbitres.size();
	     for(int i=0;i<ta;i++){
	    	 listUtilisateur.add(listArbitres.get(i));
	     }
	     
	     int t = listUtilisateur.size();
	     String data[] = new String[t];
		    //int i = 0;
		      for(int i = 0;i<t;i++ ){
		    	  data[i]=listUtilisateur.get(i).getNom()+" "+listUtilisateur.get(i).getPrenom()+" - "+listUtilisateur.get(i).getRoles();
		      }

		select_user = new JComboBox<String>(data);
		select_user.setBounds(300, 114, 200, 29);
	    panel.add(select_user);
	 	
	 	
	 		//////
			btn_retour = new JButton("Retour");
			btn_retour.addActionListener(this);
			btn_retour.setForeground(Color.BLUE);
			btn_retour.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
			btn_retour.setBounds(199, 353, 200, 29);
			panel.add(btn_retour);		
			//
			btn_appliquer = new JButton("Appliquer");
			btn_appliquer.addActionListener(this);
			btn_appliquer.setForeground(Color.BLUE);
			btn_appliquer.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
			btn_appliquer.setBounds(400, 353, 200, 29);
			panel.add(btn_appliquer);
			
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == btn_retour){dispose();}
		if(e.getSource() == btn_appliquer){
			
			
			m.setNom(textField_nom.getText());
			m.setQuantite((Integer) select_quantite.getSelectedItem());
			
			
			
			m.setId_user(listUtilisateur.get(select_user.getSelectedIndex()).getId());
			
			
			daom.ajouterMateriel(m);
			REmailUser mail = new REmailUser();
			mail.REmailUserMateriel(listUtilisateur.get(select_user.getSelectedIndex()), m);
			parent.dispose();
			MaterielDialog mf = new MaterielDialog();
			mf.setVisible(true);
			
			this.dispose();
			
		}
		
	}
	
	


}
