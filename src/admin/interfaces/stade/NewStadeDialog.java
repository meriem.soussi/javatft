package admin.interfaces.stade;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import dao.DAOStade;
import entities.Stade;
import interfaces.BgBorder;
import interfaces.Login;

public class NewStadeDialog extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	

	JTextField textField_nom;
	JTextField textField_lieux;
	JComboBox<String> select_type;
	
	DAOStade daos;
	Stade s;
	
	StadeDialog parent;
	
	JButton btn_retour;
	JButton btn_appliquer;
	
	public NewStadeDialog(DAOStade daoc, StadeDialog par){
		
		this.daos = daoc;
		s = new Stade();
		this.parent = par;
		
		
		
		this.setTitle("Nouveau Stade");
		this.setResizable(false);
		this.setBounds(100, 100, 800, 445);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(null);
		this.setModalityType(DEFAULT_MODALITY_TYPE);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 800, 445);
		this.getContentPane().add(panel);
		
		
		
		try {
		     BgBorder borde = new BgBorder(ImageIO.read(Login.class.getResource("/images/tennis05.jpeg")) );            
		     (panel).setBorder(borde);            
		     panel.setLayout(null);
		} catch (IOException e) {e.printStackTrace();}
		/////
		
		// nom
		JLabel lbNom = new JLabel("Nom: ");
		lbNom.setHorizontalAlignment(SwingConstants.RIGHT);
		lbNom.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
		lbNom.setForeground(new Color(238, 203, 51));
		lbNom.setBackground(Color.BLACK);
		lbNom.setBounds(99, 50, 200, 22);
	    panel.add(lbNom);
	    textField_nom = new JTextField();
	    
	    textField_nom.setBounds(300, 50, 200, 29);
	    panel.add(textField_nom);
	   
	    
	   // 50/110/140/170/200
	//  Lieux
	    JLabel lblLieux = new JLabel("Lieux: ");
	    lblLieux.setHorizontalAlignment(SwingConstants.RIGHT);
	    lblLieux.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblLieux.setForeground(new Color(238, 203, 51));
	    lblLieux.setBackground(Color.BLACK);
	    lblLieux.setBounds(99, 82, 200, 22);
	    panel.add(lblLieux);
	    textField_lieux = new JTextField();
	    
	    textField_lieux.setBounds(300, 82, 200, 29);
	    panel.add(textField_lieux);
	    /////////////
	 // Type
	    JLabel lblType = new JLabel("Type: ");
	    lblType.setHorizontalAlignment(SwingConstants.RIGHT);
	    lblType.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblType.setForeground(new Color(238, 203, 51));
	    lblType.setBackground(Color.BLACK);
	    lblType.setBounds(99, 114, 200, 22);
	    panel.add(lblType);
		String[] types = {"Amateur", "National", "International"};
		select_type = new JComboBox<String>(types);
		select_type.setBounds(300, 114, 200, 29);
	    panel.add(select_type);
	 	
	 	
	 		//////
			btn_retour = new JButton("Retour");
			btn_retour.addActionListener(this);
			btn_retour.setForeground(Color.BLUE);
			btn_retour.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
			btn_retour.setBounds(199, 353, 200, 29);
			panel.add(btn_retour);		
			//
			btn_appliquer = new JButton("Appliquer");
			btn_appliquer.addActionListener(this);
			btn_appliquer.setForeground(Color.BLUE);
			btn_appliquer.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
			btn_appliquer.setBounds(400, 353, 200, 29);
			panel.add(btn_appliquer);
			
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == btn_retour){dispose();}
		if(e.getSource() == btn_appliquer){
			
			
			s.setNom(textField_nom.getText());
			s.setType((String) select_type.getSelectedItem());
			s.setAdresse(textField_lieux.getText());
			
			
			daos.ajouterStade(s);
			parent.dispose();
			StadeDialog mf = new StadeDialog();
			mf.setVisible(true);
			this.dispose();
			
		}
		
	}
	
	


}
