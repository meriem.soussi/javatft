package admin.interfaces.match;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import dao.DAOCompetition;
import dao.DAOEvenement;
import dao.DAOMatch;
import dao.DAOStade;
import entities.Competition;
import entities.Evenement;
import entities.Match;
import entities.Stade;
import interfaces.BgBorder;
import interfaces.Login;
import outils.DateTimePicker;

public class MatchModifDialog extends JDialog implements ActionListener{
	private static final long serialVersionUID = 1L;
	DateTimePicker dtp_debut;
	DateTimePicker dtp_fin;
	
	SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	
	
	JComboBox<String> select_lieux_name;
	
	
	JComboBox<String> select_type;
	
	JComboBox<Integer> select_day_debut;
	JComboBox<Integer> select_month_debut;
	JComboBox<Integer> select_year_debut;
	JComboBox<Integer> select_heure_debut;
	JComboBox<Integer> select_minute_debut;
	
	JComboBox<Integer> select_day_fin;
	JComboBox<Integer> select_month_fin;
	JComboBox<Integer> select_year_fin;
	JComboBox<Integer> select_heure_fin;
	JComboBox<Integer> select_minute_fin;
	
	JComboBox<String> select_competition_name;
	
	
	JComboBox<String> select_event_name;
	
	
	DAOEvenement daoevent;
	DAOStade daostade;
	DAOCompetition daocompetition;
	
	Match m;
	DAOMatch daomatch;
	MatchDialog parent;
	
	JButton btn_retour;
	JButton btn_appliquer;
	
	List<Stade> listStades = new ArrayList<Stade>();
	List<Competition> listcompetitions = new ArrayList<Competition>();
	List<Evenement> listevents = new ArrayList<Evenement>();
	
	public MatchModifDialog(Match match, DAOMatch daom, MatchDialog par){
		
		this.daomatch = daom;
		this.m = match;
		this.parent = par;
		
		daoevent = new DAOEvenement();
		daostade = new DAOStade();
		daocompetition = new DAOCompetition();
		
		this.setTitle("Match Num�ro : "+m.getId());
		this.setResizable(false);
		this.setBounds(100, 100, 800, 445);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(null);
		this.setModalityType(DEFAULT_MODALITY_TYPE);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 800, 445);
		this.getContentPane().add(panel);
		
		
		
		try {
		     BgBorder borde = new BgBorder(ImageIO.read(Login.class.getResource("/images/tennis05.jpeg")) );            
		     (panel).setBorder(borde);            
		     panel.setLayout(null);
		} catch (IOException e) {e.printStackTrace();}
		/////
		
		// lieux
		JLabel lblLieux = new JLabel("Lieux: ");
		lblLieux.setHorizontalAlignment(SwingConstants.RIGHT);
		lblLieux.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
		lblLieux.setForeground(new Color(238, 203, 51));
		lblLieux.setBackground(Color.BLACK);
		lblLieux.setBounds(99, 50, 200, 22);
	    panel.add(lblLieux);
	    // remplissage
		
		listStades = daostade.getStades();
	    int t = listStades.size();
	    String lieux_name[] = new String[t];
		      for(int i = 0;i<t;i++ ){
		    	  lieux_name[i]=listStades.get(i).getNom();
		      }
		select_lieux_name = new JComboBox<String>(lieux_name);
		select_lieux_name.setBounds(300, 50, 200, 29);
	    panel.add(select_lieux_name);
		
		
	    
	    
	    // Type
	    JLabel lblType = new JLabel("Type: ");
	    lblType.setHorizontalAlignment(SwingConstants.RIGHT);
	    lblType.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblType.setForeground(new Color(238, 203, 51));
	    lblType.setBackground(Color.BLACK);
	    lblType.setBounds(99, 80, 200, 22);
	    panel.add(lblType);
		String[] types = {"Amateur", "National", "International"};
		select_type = new JComboBox<String>(types);
		select_type.setBounds(300, 80, 200, 29);
	    panel.add(select_type);
		
		
	 // date_debut
	    JLabel lblDate_debut = new JLabel("Date Debut: ");
	    lblDate_debut.setHorizontalAlignment(SwingConstants.RIGHT);
	    lblDate_debut.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblDate_debut.setForeground(new Color(238, 203, 51));
	    lblDate_debut.setBackground(Color.BLACK);
	    lblDate_debut.setBounds(99, 110, 200, 22);
	    panel.add(lblDate_debut);
	    Date date = new Date();
        dtp_debut = new DateTimePicker();
        dtp_debut.setFormats( DateFormat.getDateTimeInstance( DateFormat.SHORT, DateFormat.MEDIUM ) );
        dtp_debut.setTimeFormat( DateFormat.getTimeInstance( DateFormat.MEDIUM ) );
        dtp_debut.setDate(date);
        dtp_debut.setBounds(300, 110, 200, 29);
	    panel.add(dtp_debut);
	    
	    
	   
	 // date_fin
	    JLabel lblDate_fin = new JLabel("Date Fin: ");
	    lblDate_fin.setHorizontalAlignment(SwingConstants.RIGHT);
	    lblDate_fin.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblDate_fin.setForeground(new Color(238, 203, 51));
	    lblDate_fin.setBackground(Color.BLACK);
	    lblDate_fin.setBounds(99, 140, 200, 22);
	    panel.add(lblDate_fin);
	    dtp_fin = new DateTimePicker();
	    dtp_fin.setFormats( DateFormat.getDateTimeInstance( DateFormat.SHORT, DateFormat.MEDIUM ) );
	    dtp_fin.setTimeFormat( DateFormat.getTimeInstance( DateFormat.MEDIUM ) );
	    dtp_fin.setDate(date);
	    dtp_fin.setBounds(300, 140, 200, 29);
	    panel.add(dtp_fin);
		
		
	
		
		/**
		
		//  competition
	    JLabel lblCompetition = new JLabel("Competition: ");
	    lblCompetition.setHorizontalAlignment(SwingConstants.RIGHT);
	    lblCompetition.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblCompetition.setForeground(new Color(238, 203, 51));
	    lblCompetition.setBackground(Color.BLACK);
	    lblCompetition.setBounds(99, 170, 200, 22);
	    panel.add(lblCompetition);
	    // remplissage
	    
	    listcompetitions = daocompetition.getCompetitions();
	    int tc = listcompetitions.size();
	    String competition_name[] = new String[tc];
	    for(int i = 0;i<tc;i++ ){
	    	competition_name[i]=listcompetitions.get(i).getNom();
	    }
	    select_competition_name = new JComboBox<String>(competition_name);
	    select_competition_name.setBounds(300, 170, 200, 29);
	    panel.add(select_competition_name);	
	    
	   
		
		
		
		// evenement
	    JLabel lblEvent = new JLabel("Evenement: ");
	    lblEvent.setHorizontalAlignment(SwingConstants.RIGHT);
	    lblEvent.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblEvent.setForeground(new Color(238, 203, 51));
	    lblEvent.setBackground(Color.BLACK);
	    lblEvent.setBounds(99, 200, 200, 22);
	    panel.add(lblEvent);
	    // remplissage
	 	
	 	listevents = daoevent.getEvenements();
	 	int t2 = listevents.size();
	 	String event_name[] = new String[t2];
	 	for(int i = 0;i<t2;i++ ){
	 		event_name[i]=listevents.get(i).getNom();
	 	}
	 	select_event_name = new JComboBox<String>(event_name);
	 	select_event_name.setBounds(300, 200, 200, 29);
	 	panel.add(select_event_name);	    
	    //////
	    
	    */
		
		
		
	 		//////
			btn_retour = new JButton("Retour");
			btn_retour.addActionListener(this);
			btn_retour.setForeground(Color.BLUE);
			btn_retour.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
			btn_retour.setBounds(199, 353, 200, 29);
			panel.add(btn_retour);		
			//
			btn_appliquer = new JButton("Appliquer");
			btn_appliquer.addActionListener(this);
			btn_appliquer.setForeground(Color.BLUE);
			btn_appliquer.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
			btn_appliquer.setBounds(400, 353, 200, 29);
			panel.add(btn_appliquer);
			
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == btn_retour){dispose();}
		if(e.getSource() == btn_appliquer){
			
			
			
			
			
			
			Match m2 = new Match();
			
			m2.setId(m.getId());
			m2.setType((String) select_type.getSelectedItem());
			m2.setLieux(""+listStades.get(select_lieux_name.getSelectedIndex()).getId());
			
			m2.setDate_debut(dtp_debut.getDate());
			m2.setDate_fin(dtp_fin.getDate());
			
			daomatch.updateMatch(m2);
			parent.dispose();
			MatchDialog mf = new MatchDialog();
			mf.setVisible(true);
			this.dispose();
			
		}
		
	}
	
	
	
}