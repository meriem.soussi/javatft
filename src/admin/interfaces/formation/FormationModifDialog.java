package admin.interfaces.formation;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import dao.DAOFormation;
import entities.Formation;
import interfaces.BgBorder;
import interfaces.Login;
import outils.DateTimePicker;

public class FormationModifDialog extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;
	DateTimePicker dtp_debut;
	DateTimePicker dtp_fin;
	SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	
	JTextField textField_nom;
	JTextField textField_lieux;
	
	JComboBox<String> select_lieux_name;
	
	
	JComboBox<String> select_type;
	
	
	JComboBox<String> select_competition_name;
	
	
	JComboBox<String> select_event_name;
	
	
	
	
	DAOFormation daof;
	Formation f;
	
	
	
	
	FormationDialog parent;
	
	JButton btn_retour;
	JButton btn_appliquer;
	
	
	List<Formation> listFormations = new ArrayList<Formation>();
	
	
	public FormationModifDialog(Formation comp, DAOFormation daoc, FormationDialog par){
		
		this.daof = daoc;
		this.f = comp;
		this.parent = par;
		
		
		
		this.setTitle("Formation N� : "+f.getId());
		this.setResizable(false);
		this.setBounds(100, 100, 800, 445);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(null);
		this.setModalityType(DEFAULT_MODALITY_TYPE);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 800, 445);
		this.getContentPane().add(panel);
		
		
		
		try {
		     BgBorder borde = new BgBorder(ImageIO.read(Login.class.getResource("/images/tennis05.jpeg")) );            
		     (panel).setBorder(borde);            
		     panel.setLayout(null);
		} catch (IOException e) {e.printStackTrace();}
		/////
		
		// nom
		JLabel lbNom = new JLabel("Nom: ");
		lbNom.setHorizontalAlignment(SwingConstants.RIGHT);
		lbNom.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
		lbNom.setForeground(new Color(238, 203, 51));
		lbNom.setBackground(Color.BLACK);
		lbNom.setBounds(99, 50, 200, 22);
	    panel.add(lbNom);
	    textField_nom = new JTextField();
	    textField_nom.setText(f.getNom());
	    textField_nom.setBounds(300, 50, 200, 29);
	    panel.add(textField_nom);
	   
	    
	   // 50/110/140/170/200
	    
	    
	   
	 // date_debut
	    JLabel lblDate_debut = new JLabel("Date Debut: ");
	    lblDate_debut.setHorizontalAlignment(SwingConstants.RIGHT);
	    lblDate_debut.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblDate_debut.setForeground(new Color(238, 203, 51));
	    lblDate_debut.setBackground(Color.BLACK);
	    lblDate_debut.setBounds(99, 82, 200, 22);
	    panel.add(lblDate_debut);
	    Date date = new Date();
        dtp_debut = new DateTimePicker();
        dtp_debut.setFormats( DateFormat.getDateTimeInstance( DateFormat.SHORT, DateFormat.MEDIUM ) );
        dtp_debut.setTimeFormat( DateFormat.getTimeInstance( DateFormat.MEDIUM ) );
        dtp_debut.setDate(date);
        dtp_debut.setBounds(300, 82, 200, 29);
	    panel.add(dtp_debut);
	    
	    
	   
	 // date_fin
	    JLabel lblDate_fin = new JLabel("Date Fin: ");
	    lblDate_fin.setHorizontalAlignment(SwingConstants.RIGHT);
	    lblDate_fin.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblDate_fin.setForeground(new Color(238, 203, 51));
	    lblDate_fin.setBackground(Color.BLACK);
	    lblDate_fin.setBounds(99, 111, 200, 22);
	    panel.add(lblDate_fin);
	    dtp_fin = new DateTimePicker();
	    dtp_fin.setFormats( DateFormat.getDateTimeInstance( DateFormat.SHORT, DateFormat.MEDIUM ) );
	    dtp_fin.setTimeFormat( DateFormat.getTimeInstance( DateFormat.MEDIUM ) );
	    dtp_fin.setDate(date);
	    dtp_fin.setBounds(300, 112, 200, 29);
	    panel.add(dtp_fin);
		
		
		
		//  Lieux
	    JLabel lblLieux = new JLabel("Lieux: ");
	    lblLieux.setHorizontalAlignment(SwingConstants.RIGHT);
	    lblLieux.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblLieux.setForeground(new Color(238, 203, 51));
	    lblLieux.setBackground(Color.BLACK);
	    lblLieux.setBounds(99, 143, 200, 22);
	    panel.add(lblLieux);
	    textField_lieux = new JTextField();
	    textField_lieux.setText(""+f.getLieux());
	    textField_lieux.setBounds(300, 142, 200, 29);
	    panel.add(textField_lieux);
	    
		
	 	// Type
	    JLabel lblType = new JLabel("Type: ");
	    lblType.setHorizontalAlignment(SwingConstants.RIGHT);
	    lblType.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblType.setForeground(new Color(238, 203, 51));
	    lblType.setBackground(Color.BLACK);
	    lblType.setBounds(99, 173, 200, 22);
	    panel.add(lblType);
		String[] types = {"Amateur", "National", "International"};
		select_type = new JComboBox<String>(types);
		select_type.setBounds(300, 173, 200, 29);
	    panel.add(select_type);
	 	
	 	
	 	
	 	//////
	    
	    
		
		
		
	 		//////
			btn_retour = new JButton("Retour");
			btn_retour.addActionListener(this);
			btn_retour.setForeground(Color.BLUE);
			btn_retour.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
			btn_retour.setBounds(199, 353, 200, 29);
			panel.add(btn_retour);		
			//
			btn_appliquer = new JButton("Appliquer");
			btn_appliquer.addActionListener(this);
			btn_appliquer.setForeground(Color.BLUE);
			btn_appliquer.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
			btn_appliquer.setBounds(400, 353, 200, 29);
			panel.add(btn_appliquer);
			
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == btn_retour){dispose();}
		if(e.getSource() == btn_appliquer){
			
			
			
			
			
			
			Formation m2 = new Formation();
			
			m2.setId(f.getId());
			m2.setNom(textField_nom.getText());
			m2.setType((String) select_type.getSelectedItem());
			m2.setLieux(textField_lieux.getText());
			
			m2.setDate_ouverture(dtp_debut.getDate());
			m2.setDate_cloture(dtp_fin.getDate());
			daof.updateFormation(m2);
			parent.dispose();
			FormationDialog mf = new FormationDialog();
			mf.setVisible(true);
			this.dispose();
			
		}
		
	}
	
	public Date fromSelectToDate(int d, int m, int y, int h, int min){
		
		
		Calendar cal = Calendar.getInstance();
		
		cal.set(Calendar.YEAR,y);
		cal.set(Calendar.MONTH, m-1);
		cal.set(Calendar.DAY_OF_MONTH, d);

		cal.set(Calendar.HOUR_OF_DAY, h);
		cal.set(Calendar.MINUTE, min);
		cal.set(Calendar.SECOND, 0);
		

		
		
		return cal.getTime();
		
	}
	
	
}