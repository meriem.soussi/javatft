package entities;

import java.util.Date;

public class Match {
	
	private int id; // not null 11
	private String Lieux; // not null 11  c'est le id du stade
	private String type; // not null 20
	private Date date_debut;
	private Date date_fin;
	private int id_competition; // not null 11
	private int id_event; // not null 11
	
	public Match(){
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLieux() {
		return Lieux;
	}

	public void setLieux(String lieux) {
		Lieux = lieux;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getDate_debut() {
		return date_debut;
	}

	public void setDate_debut(Date date_debut) {
		this.date_debut = date_debut;
	}

	public Date getDate_fin() {
		return date_fin;
	}

	public void setDate_fin(Date date_fin) {
		this.date_fin = date_fin;
	}

	public int getId_competition() {
		return id_competition;
	}

	public void setId_competition(int id_competition) {
		this.id_competition = id_competition;
	}

	public int getId_event() {
		return id_event;
	}

	public void setId_event(int id_event) {
		this.id_event = id_event;
	}
	
	
	
	
	
}
