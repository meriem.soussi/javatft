package entities;

import java.util.Date;

public class Utilisateur {
	
	private int id; // not null
	private String nom; // not null
	private String prenom;// not null
	private int cin;// not null
	private String adresse;
	private int telephone;// not null
	private String grade;
	private byte etat;
	private String description;
	private String image;
	private int id_formation;
	private String username;// not null
	private String username_canonical;
	private String email;
	private String email_canonical;
	private byte enabled;
	private String salt;
	private String password;// not null
	private Date last_login;
	private byte locked;
	private byte expired;
	private Date expires_at;
	private String confirmation_token;
	private Date password_requested_at;
	private String roles;// not null
	private byte credentials_expired;
	private Date credentials_expire_at;
	
	private String tranche_age;// not null
	private int id_club;

	
	
	
	public Utilisateur() {
		super();
	}




	public int getId() {
		return id;
	}




	public void setId(int id) {
		this.id = id;
	}




	public String getNom() {
		return nom;
	}




	public void setNom(String nom) {
		this.nom = nom;
	}




	public String getPrenom() {
		return prenom;
	}




	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}




	public int getCin() {
		return cin;
	}




	public void setCin(int cin) {
		this.cin = cin;
	}




	public String getAdresse() {
		return adresse;
	}




	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}




	public int getTelephone() {
		return telephone;
	}




	public void setTelephone(int telephone) {
		this.telephone = telephone;
	}




	public String getGrade() {
		return grade;
	}




	public void setGrade(String grade) {
		this.grade = grade;
	}




	public byte getEtat() {
		return etat;
	}




	public void setEtat(byte etat) {
		this.etat = etat;
	}




	public String getDescription() {
		return description;
	}




	public void setDescription(String description) {
		this.description = description;
	}




	public String getImage() {
		return image;
	}




	public void setImage(String image) {
		this.image = image;
	}




	public int getId_formation() {
		return id_formation;
	}




	public void setId_formation(int id_formation) {
		this.id_formation = id_formation;
	}




	public String getUsername() {
		return username;
	}




	public void setUsername(String username) {
		this.username = username;
	}




	public String getUsername_canonical() {
		return username_canonical;
	}




	public void setUsername_canonical(String username_canonical) {
		this.username_canonical = username_canonical;
	}




	public String getEmail() {
		return email;
	}




	public void setEmail(String email) {
		this.email = email;
	}




	public String getEmail_canonical() {
		return email_canonical;
	}




	public void setEmail_canonical(String email_canonical) {
		this.email_canonical = email_canonical;
	}




	public byte getEnabled() {
		return enabled;
	}




	public void setEnabled(byte enabled) {
		this.enabled = enabled;
	}




	public String getSalt() {
		return salt;
	}




	public void setSalt(String salt) {
		this.salt = salt;
	}




	public String getPassword() {
		return password;
	}




	public void setPassword(String password) {
		this.password = password;
	}




	public Date getLast_login() {
		return last_login;
	}




	public void setLast_login(Date last_login) {
		this.last_login = last_login;
	}




	public byte getLocked() {
		return locked;
	}




	public void setLocked(byte locked) {
		this.locked = locked;
	}




	public byte getExpired() {
		return expired;
	}




	public void setExpired(byte expired) {
		this.expired = expired;
	}




	public Date getExpires_at() {
		return expires_at;
	}




	public void setExpires_at(Date expires_at) {
		this.expires_at = expires_at;
	}




	public String getConfirmation_token() {
		return confirmation_token;
	}




	public void setConfirmation_token(String confirmation_token) {
		this.confirmation_token = confirmation_token;
	}




	public Date getPassword_requested_at() {
		return password_requested_at;
	}




	public void setPassword_requested_at(Date password_requested_at) {
		this.password_requested_at = password_requested_at;
	}




	public String getRoles() {
		return roles;
	}




	public void setRoles(String roles) {
		this.roles = roles;
	}




	public byte getCredentials_expired() {
		return credentials_expired;
	}




	public void setCredentials_expired(byte credentials_expired) {
		this.credentials_expired = credentials_expired;
	}




	public Date getCredentials_expire_at() {
		return credentials_expire_at;
	}




	public void setCredentials_expire_at(Date credentials_expire_at) {
		this.credentials_expire_at = credentials_expire_at;
	}




	public String getTranche_age() {
		return tranche_age;
	}




	public void setTranche_age(String tranche_age) {
		this.tranche_age = tranche_age;
	}




	public int getId_club() {
		return id_club;
	}




	public void setId_club(int id_club) {
		this.id_club = id_club;
	}

	
	
}
