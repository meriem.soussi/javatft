package repensable.interfaces;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import dao.DAOTest;
import dao.DAOUtilisateur;
import entities.Test;
import entities.Utilisateur;
import interfaces.BgBorder;
import interfaces.Login;
import outils.DateTimePicker;

public class NewTestDialog extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	JButton btn_retour;
	JButton btn_appliquer;
	
	JComboBox<String> select_type;
	
	DateTimePicker dtp_debut;
	
	Test test;
	DAOTest daot = new DAOTest();
	
	TestsDialog parent;
	RespensableFrame firstparent;
	DAOUtilisateur daor;
	Utilisateur respensable;
	
	SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public NewTestDialog(Utilisateur res, DAOUtilisateur dao ,TestsDialog par, RespensableFrame firstpar){
		this.parent = par;
		this.respensable = res;
		this.daor = dao;
		this.firstparent = firstpar;
		
		this.setTitle("Nouveau Teste");
		this.setResizable(false);
		this.setBounds(100, 100, 800, 445);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(null);
		this.setModalityType(DEFAULT_MODALITY_TYPE);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 800, 445);
		this.getContentPane().add(panel);
		
		
		
		try {
		     BgBorder borde = new BgBorder(ImageIO.read(Login.class.getResource("/images/tennis05.jpeg")) );            
		     (panel).setBorder(borde);            
		     panel.setLayout(null);
		} catch (IOException e) {e.printStackTrace();}
		/////
		// Type
	    JLabel lblType = new JLabel("Type: ");
	    lblType.setHorizontalAlignment(SwingConstants.RIGHT);
	    lblType.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblType.setForeground(new Color(238, 203, 51));
	    lblType.setBackground(Color.BLACK);
	    lblType.setBounds(99, 50, 200, 22);
	    panel.add(lblType);
		String[] types = {"Amateur", "National", "International"};
		select_type = new JComboBox<String>(types);
		select_type.setBounds(300, 50, 200, 29);
	    panel.add(select_type);
		 
			   // 50/110/140/170/200
			    
			    
			   
	    
	    // date_debut
	    JLabel lblDate_debut = new JLabel("Date Debut: ");
	    lblDate_debut.setHorizontalAlignment(SwingConstants.RIGHT);
	    lblDate_debut.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblDate_debut.setForeground(new Color(238, 203, 51));
	    lblDate_debut.setBackground(Color.BLACK);
	    lblDate_debut.setBounds(99, 82, 200, 22);
	    panel.add(lblDate_debut);
	    Date date = new Date();
        dtp_debut = new DateTimePicker();
        dtp_debut.setFormats( DateFormat.getDateTimeInstance( DateFormat.SHORT, DateFormat.MEDIUM ) );
        dtp_debut.setTimeFormat( DateFormat.getTimeInstance( DateFormat.MEDIUM ) );
        dtp_debut.setDate(date);
        dtp_debut.setBounds(300, 82, 200, 29);
	    panel.add(dtp_debut);
	    
				
			//////
				btn_retour = new JButton("Retour");
				btn_retour.addActionListener(this);
				btn_retour.setForeground(Color.BLUE);
				btn_retour.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
				btn_retour.setBounds(199, 353, 200, 29);
				panel.add(btn_retour);		
				//
				btn_appliquer = new JButton("Appliquer");
				btn_appliquer.addActionListener(this);
				btn_appliquer.setForeground(Color.BLUE);
				btn_appliquer.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
				btn_appliquer.setBounds(400, 353, 200, 29);
				panel.add(btn_appliquer);
		
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == btn_retour){dispose();}
		if(e.getSource() == btn_appliquer){
			
			
			
			
			test = new Test();
			test.setType((String) select_type.getSelectedItem());
			test.setDate(dtp_debut.getDate());
			daot.ajouterTest(test);
			parent.dispose();
			TestsDialog td = new TestsDialog(respensable,daor,firstparent);
			td.setVisible(true);
			this.dispose();
			
		}

	}
	
	
	
}
