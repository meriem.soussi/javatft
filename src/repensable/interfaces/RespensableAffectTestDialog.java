package repensable.interfaces;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import dao.DAOTest;
import dao.DAOTest_user;
import dao.DAOUtilisateur;
import entities.Test;
import entities.Test_user;
import entities.Utilisateur;
import interfaces.BgBorder;
import interfaces.Login;

public class RespensableAffectTestDialog extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	
	JButton btn_retour;
	JButton btn_supp;
	Test test;
	DAOTest daot = new DAOTest();
	
	TestsDialog parent;
	RespensableFrame firstparent;
	DAOUtilisateur daor;
	Utilisateur respensable;
	
	DAOTest_user daotu = new DAOTest_user();
	
	Utilisateur medecin;
	DAOUtilisateur daom;
	List<Utilisateur> listMedecin=new ArrayList<Utilisateur>();
	JComboBox<String> select_medecin_name;
	
	
	public RespensableAffectTestDialog(Test t,Utilisateur res, DAOUtilisateur dao ,TestsDialog par, RespensableFrame firstpar){
		this.test=t;
		this.parent = par;
		this.respensable = res;
		this.daor = dao;
		this.firstparent = firstpar;
		
		this.setTitle("Supprimer "+test.getId());
		this.setResizable(false);
		this.setBounds(100, 100, 400, 200);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(null);
		this.setModalityType(DEFAULT_MODALITY_TYPE);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 400, 200);
		this.getContentPane().add(panel);
		
		
		
		try {
		     BgBorder borde = new BgBorder(ImageIO.read(Login.class.getResource("/images/tennis06.jpg")) );            
		     (panel).setBorder(borde);            
		     panel.setLayout(null);
		} catch (IOException e) {e.printStackTrace();}
		/////
		
		ImageIcon img = new ImageIcon(this.getClass().getResource("/images/Question-sign.png"));
	    //mageIcon image = new ImageIcon("C:/Documents and Settings/user/Desktop/hi/xD/JavaApplication2/image.png");
	    JLabel imagelabel = new JLabel(img);
	    imagelabel.setBounds(20, 35, 80, 80);
	    panel.add(imagelabel);
		
		JLabel msg = new JLabel("Voulez vous Supprimer ");
		msg.setHorizontalAlignment(SwingConstants.LEFT);
		msg.setFont(new Font("Lucida Blackletter", Font.PLAIN, 18));
		msg.setForeground(Color.BLACK);
		msg.setBackground(Color.BLACK);
		msg.setBounds(120, 40, 250, 30);
	    panel.add(msg);
	    
	 // Gamer
	 		JLabel lblMedecin = new JLabel("Medecin: ");
	 		lblMedecin.setHorizontalAlignment(SwingConstants.RIGHT);
	 		lblMedecin.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	 		lblMedecin.setForeground(Color.BLACK);
	 		lblMedecin.setBackground(Color.BLACK);
	 		lblMedecin.setBounds(10, 50, 130, 22);
	 	    panel.add(lblMedecin);
	 		
	 		daom = new DAOUtilisateur();
	 		listMedecin= daom.getMedecins();
	 		int te = listMedecin.size();
	 		String medecin_name[] = new String[te];
	 		      for(int i = 0;i<te;i++ ){
	 		    	  medecin_name[i]=listMedecin.get(i).getNom();
	 		      }
	 		select_medecin_name = new JComboBox<String>(medecin_name);
	 		select_medecin_name.setBounds(150, 45, 200, 29);
	 		panel.add(select_medecin_name);
		
		
		
		
		btn_retour = new JButton("Non");
		btn_retour.addActionListener(this);
		btn_retour.setForeground(Color.BLUE);
		btn_retour.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
		btn_retour.setBounds(99, 130, 100, 29);
	    panel.add(btn_retour);
	    
	    btn_supp = new JButton("Oui");
	    btn_supp.addActionListener(this);
	    btn_supp.setForeground(Color.BLUE);
	    btn_supp.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
	    btn_supp.setBounds(200, 130, 100, 29);
	    panel.add(btn_supp);
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		medecin = daom.getMedecinById(listMedecin.get(select_medecin_name.getSelectedIndex()).getId());
		 
		DAOTest_user daotu = new DAOTest_user();
		Test_user tu = new Test_user();
		tu.setId_test(test.getId());
		tu.setId_user(medecin.getId());		
		daotu.ajouterTest_user(tu);
		parent.dispose();
		TestsDialog td = new TestsDialog(respensable,daor,firstparent);
		td.setVisible(true);
		this.dispose();

	}

}
