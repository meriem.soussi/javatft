package interfaces;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.text.AbstractDocument;

import arbitre.interfaces.ArbitreFrame;
import dao.DAOUtilisateur;
import entities.Utilisateur;
import filters.LengthFilter;
import filters.NumericAndLengthFilter;
import gamer.interfaces.GamerFrame;
import medecin.interfaces.MedecinFrame;
import repensable.interfaces.RespensableFrame;
import visitor.interfaces.VisitorFrame;

public class Login extends JFrame implements ActionListener{

	private static final long serialVersionUID = 1L;

	JButton btnInsription;
	JButton btnLogin;
	JTextField textField_UserName;
	private JPasswordField passwordField_Password;
	
	final JLabel lblcestVide_UserName;
	final JLabel lblcestVide_Password;
	final JComboBox<String> comboBox;
	
	ImageIcon iconExc = new ImageIcon(getClass().getResource("/images/Exception.png"));
	ImageIcon iconWar = new ImageIcon(getClass().getResource("/images/ERROR35.png"));
	ImageIcon iconSucc = new ImageIcon(getClass().getResource("/images/Succes.png"));

	public Login(){
		
		
		this.setTitle("TFT - Bienvenue");
		this.setResizable(false);
		this.setBounds(100, 100, 741, 463);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(null);
		LengthFilter lengthfilter = new LengthFilter(20);
		NumericAndLengthFilter numericandlengthfilter = new NumericAndLengthFilter(20);
		
		
		// bienvenue 
		final JPanel panel_Bienvenu = new JPanel();
		panel_Bienvenu.setBackground(new Color(0, 153, 51));
		panel_Bienvenu.setBorder(new LineBorder(Color.WHITE, 4));
		panel_Bienvenu.setBounds(64, 27, 623, 67);
		this.getContentPane().add(panel_Bienvenu);
		panel_Bienvenu.setLayout(null);
		
		JLabel lblBienvenue = new JLabel("Bienvenue a la Federation Tunisi�nne de Tennis");
		lblBienvenue.setBounds(6, 20, 611, 32);
		panel_Bienvenu.add(lblBienvenue);
		lblBienvenue.setForeground(Color.WHITE);
		lblBienvenue.setFont(new Font("Lucida Blackletter", Font.PLAIN, 24));
		lblBienvenue.setHorizontalAlignment(SwingConstants.CENTER);
		//
		
		// le panel main
		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(Color.WHITE, 4));
		panel.setBackground(new Color(0, 153, 51));
		panel.setBounds(123, 149, 496, 242);
		this.getContentPane().add(panel);
		panel.setLayout(null);
		
		textField_UserName = new JTextField();
		( (AbstractDocument) textField_UserName.getDocument()).setDocumentFilter(numericandlengthfilter);
		textField_UserName.setBounds(162, 39, 186, 28);
		panel.add(textField_UserName);
		textField_UserName.setColumns(10);
		
		JLabel lblUserName = new JLabel("N� CIN:");
		lblUserName.setAlignmentX(LEFT_ALIGNMENT);
		lblUserName.setForeground(Color.WHITE);
		lblUserName.setFont(new Font("Lucida Blackletter", Font.PLAIN, 18));
		lblUserName.setBounds(75, 39, 102, 22);
		panel.add(lblUserName);
		
		passwordField_Password = new JPasswordField();
		( (AbstractDocument) passwordField_Password.getDocument()).setDocumentFilter(lengthfilter);
		passwordField_Password.setBounds(162, 79, 186, 28);
		panel.add(passwordField_Password);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setForeground(Color.WHITE);
		lblPassword.setFont(new Font("Lucida Blackletter", Font.PLAIN, 18));
		lblPassword.setBounds(52, 78, 90, 22);
		panel.add(lblPassword);
		
		btnLogin = new JButton("Login");
		btnLogin.addActionListener(this);
		btnLogin.setBackground(new Color(50, 205, 50));
		btnLogin.setForeground(Color.BLACK);
		btnLogin.setFont(new Font("Lucida Bright", Font.PLAIN, 18));
		btnLogin.setBounds(187, 133, 117, 29);
		panel.add(btnLogin);
		
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(50, 205, 50));
		panel_1.setBounds(32, 187, 427, 38);
		panel.add(panel_1);
		
		JLabel lblSiVous = new JLabel("Si vous n'etes pas inscrit inscrivez vous");
		panel_1.add(lblSiVous);
		lblSiVous.setForeground(new Color(255, 255, 255));
		
		btnInsription = new JButton("Insription");
		btnInsription.addActionListener(this);
		btnInsription.setForeground(new Color(178, 34, 34));
		panel_1.add(btnInsription);
		
		String[] d = {"Administrateur", "Visiteur", "Respensable", "Medecin", "Joueur", "Arbitre"};
		comboBox = new JComboBox<String>(d);
		
		comboBox.setBounds(360, 41, 111, 27);
		panel.add(comboBox);
		
		
		
		
		lblcestVide_UserName = new JLabel("* C'est vide");
		lblcestVide_UserName.setForeground(Color.RED);
		lblcestVide_UserName.setFont(new Font("Lucida Grande", Font.PLAIN, 10));
		lblcestVide_UserName.setBounds(296, 23, 52, 16);
		lblcestVide_UserName.setVisible(false);
		panel.add(lblcestVide_UserName);
		
		lblcestVide_Password = new JLabel("* C'est vide");
		lblcestVide_Password.setForeground(Color.RED);
		lblcestVide_Password.setFont(new Font("Lucida Grande", Font.PLAIN, 10));
		lblcestVide_Password.setBounds(296, 65, 52, 16);
		lblcestVide_Password.setVisible(false);
		panel.add(lblcestVide_Password);
		//
		
		
		// background
		JLabel lbl_Imagen = new JLabel(" ");
		lbl_Imagen.setBounds(0, 0, 767, 477);
		lbl_Imagen.setIcon(new ImageIcon(Login.class
				.getResource("/images/Tennis1.jpeg")));
		this.getContentPane().add(lbl_Imagen);
		lbl_Imagen.setLocation(new Point(1,2)); 
		//
		
		
		// le panel 2
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(1, 2, 740, 439);
		this.getContentPane().add(panel_2);
		//
		
		
		
	}
	
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()==btnLogin ){
			String role = (String) comboBox.getSelectedItem();
			
			lblcestVide_UserName.setVisible(false);
			lblcestVide_Password.setVisible(false);
			
		//--------------------------------------------- test si vide ou non
			if (!textField_UserName.getText().isEmpty()){
				if(!passwordField_Password.getPassword().toString().isEmpty()){
					DAOUtilisateur daou  = new DAOUtilisateur();
					Utilisateur u = new Utilisateur();
					char[] passwordchar = passwordField_Password.getPassword();
					String password = String.valueOf(passwordchar);
			//----------------------------------------------
					u.setCin(Integer.parseInt(textField_UserName.getText()));
					u.setPassword(password);
					u.setRoles(role);
				 //-----------------------------------------------	
						if(daou.exist(u)){
							Utilisateur u1 = new Utilisateur();
							u1 = daou.getUserByLogin(u);
							entrer(u1);
						}else JOptionPane.showMessageDialog(this,"Param�tres incorrectes !!\nVeillez v�rifier vous param�tres d'acc�s", "Erreur", JOptionPane.ERROR_MESSAGE);
						//----------------------------------------------
					}
					else {JOptionPane.showMessageDialog(this,"Mot de passe vide !!", "Erreur", JOptionPane.ERROR_MESSAGE);passwordField_Password.requestFocus();lblcestVide_Password.setVisible(true);}
					
				}else {JOptionPane.showMessageDialog(this,"Login vide !!", "Erreur", JOptionPane.ERROR_MESSAGE);textField_UserName.requestFocus();lblcestVide_UserName.setVisible(true);}
				//--------------------------------------------------------------------------------------------------------	
		}// end of btnlogin 
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
		
				
		
		if(e.getSource()==btnInsription){
			
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						InscriptionFrame inf = new InscriptionFrame();
						inf.setVisible(true);
						
					} catch (Exception e) {e.printStackTrace();
					}
				}
			});
			
			
			//dispose();
			
		}//
	
	
	
	
	}	// end of listeners
	
	public void entrer(Utilisateur u1){
		String s = u1.getRoles();
		switch (s) 
		{
			case "Administrateur" : 		AdminFrame af = new AdminFrame(u1);af.setVisible(true);break;
			case "Respensable" : 		RespensableFrame rf = new RespensableFrame(u1);rf.setVisible(true);break;
			case "Visiteur" : 	VisitorFrame vf = new VisitorFrame(u1);vf.setVisible(true);break;
			case "Medecin" :	MedecinFrame mf = new MedecinFrame(u1);mf.setVisible(true);break;
			case "Joueur" : 	GamerFrame gf = new GamerFrame(u1);gf.setVisible(true);break;
			case "Arbitre" : 	ArbitreFrame arbf = new ArbitreFrame(u1);arbf.setVisible(true);break;
			default:break;
		}
		dispose();
		
		
	}
	
	
}
