package mail;

import java.text.SimpleDateFormat;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JOptionPane;

import entities.Match;
import entities.Materiel;
import entities.Test;
import entities.Utilisateur;

public class REmailUser {
	SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	private class SMTPAuthenticator extends Authenticator
    {
        private PasswordAuthentication authentication;

        public SMTPAuthenticator(String login, String password)
        {
             authentication = new PasswordAuthentication(login, password);
        }

        @Override
        protected PasswordAuthentication getPasswordAuthentication()
        {
             return authentication;
        }
    }
	
	public REmailUser(){
		
	}
	public void REmailUserTest(Utilisateur u, Test t){
		
		
		
		String from = "tfttennistunisie@gmail.com";
		String to = u.getEmail();
		String subject = "TFT - Notification pour passer un Teste.";
		String message = "Bonjour Me/Mlle "+u.getNom()+" "+u.getPrenom()+".\n"+"Vous etes invit� � passer le Teste N� "+t.getId()+" le "+sdf.format(t.getDate());
		
		
		
		String login = "tfttennistunisie@gmail.com";
		String password = "tfttennis2016";

		Properties props = new Properties();
		props.setProperty("mail.host", "smtp.gmail.com");
		props.setProperty("mail.smtp.port", "587");
		props.setProperty("mail.smtp.auth", "true");
		props.setProperty("mail.smtp.starttls.enable", "true");

		Authenticator auth = new SMTPAuthenticator(login, password);

		Session session = Session.getInstance(props, auth);

		MimeMessage msg = new MimeMessage(session);

         try
         {
		    msg.setText(message);
		    msg.setSubject(subject);
		    msg.setFrom(new InternetAddress(from));
		    msg.addRecipient(Message.RecipientType.TO,
		    new InternetAddress(to));
		    Transport.send(msg);
		    JOptionPane.showMessageDialog(null, "Email Envoy�");
         }
         catch (MessagingException ex)
         {
        	 ex.printStackTrace();
		   JOptionPane.showMessageDialog(null, "Erreur d'ennvoie");
		    //Logger.getLogger(MailClient.class.getName()).log(Level.SEVERE, null, ex);
         }
		
		
	          
	         
	
	}	
		
		
		
	
	
	public void REmailUserMateriel(Utilisateur u, Materiel t){
		String from = "tfttennistunisie@gmail.com";
		String to = u.getEmail();
		String subject = "TFT - Notification pour Materiels.";
		String message = "Bonjour Me/Mlle "+u.getNom()+" "+u.getPrenom()+".\n"+"Nous Vous a affect� "+t.getQuantite()+" "+t.getNom();
		
		
		
		String login = "tfttennistunisie@gmail.com";
		String password = "tfttennis2016";

		Properties props = new Properties();
		props.setProperty("mail.host", "smtp.gmail.com");
		props.setProperty("mail.smtp.port", "587");
		props.setProperty("mail.smtp.auth", "true");
		props.setProperty("mail.smtp.starttls.enable", "true");

		Authenticator auth = new SMTPAuthenticator(login, password);

		Session session = Session.getInstance(props, auth);

		MimeMessage msg = new MimeMessage(session);

         try
         {
		    msg.setText(message);
		    msg.setSubject(subject);
		    msg.setFrom(new InternetAddress(from));
		    msg.addRecipient(Message.RecipientType.TO,
		    new InternetAddress(to));
		    Transport.send(msg);
		    JOptionPane.showMessageDialog(null, "Email Envoy�");
         }
         catch (MessagingException ex)
         {
        	 ex.printStackTrace();
		   JOptionPane.showMessageDialog(null, "Erreur d'ennvoie");
		    //Logger.getLogger(MailClient.class.getName()).log(Level.SEVERE, null, ex);
         }
		
	}
	
	
	public void REmailUserMatch(Utilisateur u, Match m){
		String from = "tfttennistunisie@gmail.com";
		String to = u.getEmail();
		String subject = "TFT - Notification De Match.";
		String message = "Bonjour Me/Mlle "+u.getNom()+" "+u.getPrenom()+".\n"+"Vous etes invit� au match N� "+m.getId()+" le "+sdf.format(m.getDate_debut());
		
		
		String login = "tfttennistunisie@gmail.com";
		String password = "tfttennis2016";

		Properties props = new Properties();
		props.setProperty("mail.host", "smtp.gmail.com");
		props.setProperty("mail.smtp.port", "587");
		props.setProperty("mail.smtp.auth", "true");
		props.setProperty("mail.smtp.starttls.enable", "true");

		Authenticator auth = new SMTPAuthenticator(login, password);

		Session session = Session.getInstance(props, auth);

		MimeMessage msg = new MimeMessage(session);

         try
         {
		    msg.setText(message);
		    msg.setSubject(subject);
		    msg.setFrom(new InternetAddress(from));
		    msg.addRecipient(Message.RecipientType.TO,
		    new InternetAddress(to));
		    Transport.send(msg);
		    JOptionPane.showMessageDialog(null, "Email Envoy�");
         }
         catch (MessagingException ex)
         {
        	 ex.printStackTrace();
		   JOptionPane.showMessageDialog(null, "Erreur d'ennvoie");
		    //Logger.getLogger(MailClient.class.getName()).
		    //log(Level.SEVERE, null, ex);
         }
	}
	
	
}
