package medecin.interfaces;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import dao.DAOTest;
import dao.DAOTest_user;
import dao.DAOUtilisateur;
import entities.Test;
import entities.Test_user;
import entities.Utilisateur;
import interfaces.BgBorder;
import interfaces.Login;
import outils.DateTimePicker;

public class NewTestDialog extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	
	DateTimePicker dtp_debut;
	
	JComboBox<String> select_type;
	
	Test test;
	DAOTest daot;
	MedecinFrame parent;
	DAOUtilisateur daom;
	Utilisateur medecin;
	JButton btn_retour;
	JButton btn_appliquer;
	/////////
	Test_user test_user = new Test_user();
	
	/////
	public NewTestDialog(Utilisateur med, DAOUtilisateur dao, MedecinFrame par){
		this.medecin=med;
		this.daom = dao;
		this.daot = new DAOTest();
		test = new Test();
		this.parent = par;
		
		
		
		this.setTitle("Nouveau Match");
		this.setResizable(false);
		this.setBounds(100, 100, 800, 445);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(null);
		this.setModalityType(DEFAULT_MODALITY_TYPE);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 800, 445);
		this.getContentPane().add(panel);
		
		
		
		try {
		     BgBorder borde = new BgBorder(ImageIO.read(Login.class.getResource("/images/tennis05.jpeg")) );            
		     (panel).setBorder(borde);            
		     panel.setLayout(null);
		} catch (IOException e) {e.printStackTrace();}
		/////
		
		// lieux
		JLabel lblLieux = new JLabel("Lieux: ");
		lblLieux.setHorizontalAlignment(SwingConstants.RIGHT);
		lblLieux.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
		lblLieux.setForeground(new Color(238, 203, 51));
		lblLieux.setBackground(Color.BLACK);
		lblLieux.setBounds(99, 50, 200, 22);
	    panel.add(lblLieux);
	   
	    // Type
	    JLabel lblType = new JLabel("Type: ");
	    lblType.setHorizontalAlignment(SwingConstants.RIGHT);
	    lblType.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblType.setForeground(new Color(238, 203, 51));
	    lblType.setBackground(Color.BLACK);
	    lblType.setBounds(99, 80, 200, 22);
	    panel.add(lblType);
		String[] types = {"Amateur", "National", "International"};
		select_type = new JComboBox<String>(types);
		select_type.setBounds(300, 80, 200, 29);
	    panel.add(select_type);
		
	    // date_debut
	    JLabel lblDate_debut = new JLabel("Date Debut: ");
	    lblDate_debut.setHorizontalAlignment(SwingConstants.RIGHT);
	    lblDate_debut.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblDate_debut.setForeground(new Color(238, 203, 51));
	    lblDate_debut.setBackground(Color.BLACK);
	    lblDate_debut.setBounds(99, 110, 200, 22);
	    panel.add(lblDate_debut);
	    Date date = new Date();
        dtp_debut = new DateTimePicker();
        dtp_debut.setFormats( DateFormat.getDateTimeInstance( DateFormat.SHORT, DateFormat.MEDIUM ) );
        dtp_debut.setTimeFormat( DateFormat.getTimeInstance( DateFormat.MEDIUM ) );
        dtp_debut.setDate(date);
        dtp_debut.setBounds(300, 110, 200, 29);
	    panel.add(dtp_debut);
	    
		
	    
		
	 
		
		
	 		//////
			btn_retour = new JButton("Retour");
			btn_retour.addActionListener(this);
			btn_retour.setForeground(Color.BLUE);
			btn_retour.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
			btn_retour.setBounds(199, 353, 200, 29);
			panel.add(btn_retour);		
			//
			btn_appliquer = new JButton("Appliquer");
			btn_appliquer.addActionListener(this);
			btn_appliquer.setForeground(Color.BLUE);
			btn_appliquer.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
			btn_appliquer.setBounds(400, 353, 200, 29);
			panel.add(btn_appliquer);
			
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == btn_retour){dispose();}
		if(e.getSource() == btn_appliquer){
			

			test.setType((String) select_type.getSelectedItem());
			test.setDate(dtp_debut.getDate());
			daot.ajouterTest(test);
			DAOTest_user daotu = new DAOTest_user();
			test_user.setId_test(test.getId());
			test_user.setId_user(medecin.getId());
			daotu.ajouterTest_user(test_user);
			
			parent.dispose();
			this.dispose();
			MedecinTestDialog mf = new MedecinTestDialog(medecin,daom,parent);
			mf.setVisible(true);
			
			
		}
		
	}
	
	public Date fromSelectToDate(int d, int m, int y, int h, int min){
		
		
		Calendar cal = Calendar.getInstance();
		
		cal.set(Calendar.YEAR,y);
		cal.set(Calendar.MONTH, m-1);
		cal.set(Calendar.DAY_OF_MONTH, d);

		cal.set(Calendar.HOUR_OF_DAY, h);
		cal.set(Calendar.MINUTE, min);
		cal.set(Calendar.SECOND, 0);
		
		return cal.getTime();
		
	}
	
	


}
