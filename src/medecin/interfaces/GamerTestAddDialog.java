package medecin.interfaces;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import dao.DAOTest_user;
import dao.DAOUtilisateur;

import entities.Test;
import entities.Test_user;
import entities.Utilisateur;
import interfaces.BgBorder;
import interfaces.Login;
import mail.REmailUser;

public class GamerTestAddDialog extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;
	
	JButton btn_retour;
	JButton btn_affect;
	Utilisateur gamer;
	
	Utilisateur medecin;
	DAOUtilisateur daom;
	
	Test test;
	DAOTest_user daotu;
	
	List<Utilisateur> listGamer=new ArrayList<Utilisateur>();
	
	MedecinFrame parent;
	
	JComboBox<String> select_gamer_name;
	
	
	
	public GamerTestAddDialog(Test te     , Utilisateur med, DAOUtilisateur dao, MedecinFrame par) {
		
		this.test = te;
		this.medecin = med;
		this.daom = dao;
		this.parent = par;
		
		this.setTitle("Affecter au Test N� "+test.getId());
		this.setResizable(false);
		this.setBounds(100, 100, 400, 200);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(null);
		this.setModalityType(DEFAULT_MODALITY_TYPE);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 400, 200);
		this.getContentPane().add(panel);
		
		
		
		
		
		try {
		     BgBorder borde = new BgBorder(ImageIO.read(Login.class.getResource("/images/tennis06.jpg")) );            
		     (panel).setBorder(borde);            
		     panel.setLayout(null);
		} catch (IOException e) {e.printStackTrace();}
		/////
		
		
		// Gamer
		JLabel lblGamer = new JLabel("Joueur: ");
		lblGamer.setHorizontalAlignment(SwingConstants.RIGHT);
		lblGamer.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
		lblGamer.setForeground(Color.BLACK);
		lblGamer.setBackground(Color.BLACK);
		lblGamer.setBounds(10, 50, 130, 22);
	    panel.add(lblGamer);
		
	    

		daotu = new DAOTest_user();
		gamer = new Utilisateur();
		listGamer = new ArrayList<Utilisateur>();
		listGamer= daom.getGamers();
		int t = listGamer.size();
		String gamer_name[] = new String[t];
		      for(int i = 0;i<t;i++ ){
		    	  gamer_name[i]=listGamer.get(i).getNom()+" "+listGamer.get(i).getPrenom();
		      }
		select_gamer_name = new JComboBox<String>(gamer_name);
		select_gamer_name.setBounds(150, 45, 200, 29);
		panel.add(select_gamer_name);
		
		
	    
		
		
		
		
		btn_retour = new JButton("Retour");
		btn_retour.addActionListener(this);
		btn_retour.setForeground(Color.BLUE);
		btn_retour.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
		btn_retour.setBounds(99, 130, 100, 29);
	    panel.add(btn_retour);
	    
	    btn_affect = new JButton("Affecter");
	    btn_affect.addActionListener(this);
	    btn_affect.setForeground(Color.BLUE);
	    btn_affect.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
	    btn_affect.setBounds(200, 130, 100, 29);
	    panel.add(btn_affect);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource()== btn_retour){dispose();}
		
		if(e.getSource() == btn_affect){
			gamer = daom.getGamerById(listGamer.get(select_gamer_name.getSelectedIndex()).getId());
			//parent.dispose();
			
			Test_user tu = new Test_user();
			tu.setId_test(test.getId());
			tu.setId_user(gamer.getId());
			
			
			daotu.ajouterTest_user(tu);
			REmailUser mail = new REmailUser();
			mail.REmailUserTest(gamer,test);
			//MatchDialog mat = new MatchDialog();
			//mat.setVisible(true);
			this.dispose();
		}

	}

	

}
