package visitor.interfaces;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import dao.DAOClub;
import entities.Club;
import entities.Utilisateur;
import interfaces.BgBorder;
import interfaces.Login;

public class VisitorGamerShowDialog extends JDialog implements ActionListener {
	private static final long serialVersionUID = 1L;

	JButton btn_retour;
	
	DAOClub daoc = new DAOClub();
	
	public VisitorGamerShowDialog(Utilisateur u){
		this.setTitle(u.getNom()+" "+u.getPrenom());
		this.setResizable(false);
		this.setBounds(100, 100, 800, 445);
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.getContentPane().setLayout(null);
		this.setModalityType(DEFAULT_MODALITY_TYPE);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 800, 445);
		this.getContentPane().add(panel);
		
		
		
		try {
		     BgBorder borde = new BgBorder(ImageIO.read(Login.class.getResource("/images/tennis05.jpeg")) );            
		     (panel).setBorder(borde);            
		     panel.setLayout(null);
		} catch (IOException e) {e.printStackTrace();}
		/////
		
		JLabel lblNom = new JLabel("Nom: "+u.getNom());
	    lblNom.setHorizontalAlignment(SwingConstants.LEFT);
	    lblNom.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblNom.setForeground(new Color(238, 203, 51));
	    lblNom.setBackground(Color.BLACK);
	    lblNom.setBounds(50, 50, 400, 22);
	    panel.add(lblNom);
		//
	    JLabel lblPrenom = new JLabel("Prenom: "+u.getPrenom());
	    lblPrenom.setHorizontalAlignment(SwingConstants.LEFT);
	    lblPrenom.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
	    lblPrenom.setForeground(new Color(238, 203, 51));
	    lblPrenom.setBackground(Color.BLACK);
	    lblPrenom.setBounds(50, 80, 400, 22);
	    panel.add(lblPrenom);
	    //
	    if(u.getId_club() == 0){
	    	JLabel lblClub = new JLabel("Club: Libre");
	    	lblClub.setHorizontalAlignment(SwingConstants.LEFT);
		    lblClub.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
		    lblClub.setForeground(new Color(238, 203, 51));
		    lblClub.setBackground(Color.BLACK);
		    lblClub.setBounds(50, 110, 400, 22);
		    panel.add(lblClub);
	    }
        else{ 
        	Club c = daoc.getClubById(u.getId_club());  
        	JLabel lblClub = new JLabel("Club: "+c.getNom());
        	lblClub.setHorizontalAlignment(SwingConstants.LEFT);
    	    lblClub.setFont(new Font("Monotype Corsiva", Font.BOLD, 26));
    	    lblClub.setForeground(new Color(238, 203, 51));
    	    lblClub.setBackground(Color.BLACK);
    	    lblClub.setBounds(50, 110, 400, 22);
    	    panel.add(lblClub);
        }
	    
	    
	    
	    
	     
	    
	    ImageIcon img = null;
		try {
			img = new ImageIcon(ImageIO.read(new File("C:/imagesUtilisateurs/"+u.getCin()+".jpg")));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    if(img!=null){
	    	JLabel imagelabel = new JLabel(img);
		    imagelabel.setBounds(500, 50, 300, 300);
		    panel.add(imagelabel);
	    }
	    else{
	    	JLabel imagelabel = new JLabel("Pas d'image");
		    imagelabel.setBounds(500, 50, 300, 300);
		    panel.add(imagelabel);
	    }
	   
	    
		
			//////
			btn_retour = new JButton("Retour");
			btn_retour.addActionListener(this);
			btn_retour.setForeground(Color.BLUE);
			btn_retour.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
			btn_retour.setBounds(300, 353, 200, 29);
		    panel.add(btn_retour);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btn_retour) {this.dispose();}

	}
	
}
