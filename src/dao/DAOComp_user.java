package dao;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import entities.Comp_user;

public class DAOComp_user {
	
	Statement s;
	ResultSet r;
		
	public DAOComp_user() {
		try{
			s=Connect.getInstance().createStatement();
		}catch (Exception e) {e.printStackTrace();}
	}
	
	
	
	public void ajouterComp_user(Comp_user u){
		try{
			s.executeUpdate("INSERT INTO comp_user (id_competition,id_user) VALUES ('"
														+u.getId_competition()+"','"
														+u.getId_user()+"'"
														+ ");"   );
		}catch (Exception e){e.printStackTrace();}
		
	}
	
	
	
	public List<Comp_user>getComp_users() {
		List<Comp_user> lm = new ArrayList<Comp_user>();
		try{
			r=s.executeQuery(" select * from comp_user");
			while(r.next()){
				Comp_user u = new Comp_user();
				
				u.setId(r.getInt("id"));
				u.setId_competition(r.getInt("id_competition"));
				u.setId_user(r.getInt("id_user"));
				
				lm.add(u);
			}
				return lm;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	
	
	
	
	
	public void deleteComp_user(Comp_user u){
		try{
			s.executeUpdate("DELETE FROM comp_user where id='"+u.getId()+"' ");
		}catch (Exception e){e.printStackTrace();}
	}
	
	
	
	public void updateComp_user(Comp_user u){
		try{
			s.executeUpdate("update comp_user set id_competition= '"+u.getId_competition()
													+"' ,id_user='"+u.getId_user()
													+"' where id= '"+u.getId()+"'    ");				
		}catch (Exception e){e.printStackTrace();}
	}
	
}
