package dao;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import entities.Resultat;

public class DAOResultat {
	
	Statement s;
	ResultSet r;
		
	public DAOResultat() {
		try{
			s=Connect.getInstance().createStatement();
		}catch (Exception e) {e.printStackTrace();}
	}
	
	
	
	public void ajouterResultat(Resultat u){
		try{
			s.executeUpdate("INSERT INTO resultat (description,date,type,id_user) VALUES ('"
														+u.getDescription()+"','"
														+u.getDate()+"','"
														+u.getType()+"','"
														+u.getId_user()+"'"
														+ ");"   );
		}catch (Exception e){e.printStackTrace();}
		
	}
	
	
	
	public List<Resultat>getResultats() {
		List<Resultat> lm = new ArrayList<Resultat>();
		try{
			r=s.executeQuery(" select * from resultat");
			while(r.next()){
				Resultat u = new Resultat();
				
				u.setId(r.getInt("id"));
				u.setDescription(r.getString("description"));
				u.setDate(r.getDate("date"));
				u.setType(r.getString("type"));
				u.setId_user(r.getInt("id_user"));
				
				

				lm.add(u);
			}
				return lm;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	
	
	
	
	
	public void deleteResultat(Resultat u){
		try{
			s.executeUpdate("DELETE FROM resultat where id='"+u.getId()+"' ");
		}catch (Exception e){e.printStackTrace();}
	}
	
	
	
	public void updateResultat(Resultat u){
		try{
			s.executeUpdate("update resultat set description= '"+u.getDescription()
																+"' ,date= '"+u.getDate()
																+"' ,type='"+u.getType()
																+"' ,id_user='"+u.getId_user()
																+"' where id= '"+u.getId()+"'    ");				
		}catch (Exception e){e.printStackTrace();}
	}
	
	
	
}
