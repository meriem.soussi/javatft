package dao;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import entities.Utilisateur;

public class DAOUtilisateur {
	
	Statement s;
	ResultSet r;
	List<Utilisateur> listUtilisateur=new ArrayList<Utilisateur>();
	
	public DAOUtilisateur() {
		try{
			s=Connect.getInstance().createStatement();
		}catch (Exception e) {e.printStackTrace();}
	}
	
	public  boolean exist(Utilisateur u){
		boolean b=false;
		try {
			//System.out.println("Log.dao : "+u.getGrade());
			//System.out.println("Log.dao : "+u.getCin());
			//System.out.println("Log.dao : "+u.getPassword());
			r=s.executeQuery("select * from utilisateur where cin='"+u.getCin()+"' and password= '"+u.getPassword()+"' and roles= '"+u.getRoles()+"'  ");
			b=r.next();
		} catch (SQLException e) {e.printStackTrace();}
		return b;
	}
	
	public Utilisateur getUserByLogin(Utilisateur u){
		Utilisateur u1 = new Utilisateur();
		try {
			r=s.executeQuery("select * from utilisateur where cin='"+u.getCin()+"' and password= '"+u.getPassword()+"' and roles= '"+u.getRoles()+"'  ");
			r.next();
			u1.setId(r.getInt("id"));
			u1.setNom(r.getString("nom"));
			u1.setPrenom(r.getString("prenom"));
			u1.setCin(r.getInt("cin"));
			u1.setAdresse(r.getString("adresse"));
			u1.setTelephone(r.getInt("telephone"));
			
			u1.setGrade(r.getString("grade"));
			u1.setEmail(r.getString("email"));
			u1.setRoles(r.getString("roles"));
			
			u1.setUsername(r.getString("username"));
			u1.setPassword(r.getString("password"));
			u1.setDescription(r.getString("description"));
			
			u1.setTranche_age(r.getString("tranche_age"));
			u1.setId_club(r.getInt("id_club"));
			
		
		} catch (SQLException e) {e.printStackTrace();}
		
		return u1;
		
	}
	
	
	public void ajouterUtilisateur(Utilisateur u){
		try{
			s.executeUpdate("INSERT INTO utilisateur (nom,prenom,cin,email,telephone,adresse,username,password,image,roles,description, username_canonical, email_canonical, enabled, salt, locked, expired, credentials_expired, tranche_age) VALUES ('"
														+u.getNom()+"','"
														+u.getPrenom()+"','"
														+u.getCin()+"','"
														+u.getEmail()+"','"
														+u.getTelephone()+"','"
														+u.getAdresse()+"','"
														+u.getUsername()+"','"
														+u.getPassword()+"','"
														+u.getImage()+"','"
														+u.getRoles()+"','"														
														+u.getDescription()+"','"
														
														+u.getUsername_canonical()+"','"
														+u.getEmail_canonical()+"','"
														+u.getEnabled()+"','"
														+u.getSalt()+"','"
														+u.getLocked()+"','"
														+u.getExpired()+"','"
																											
														+u.getCredentials_expired()+"','"
																												
														
														+u.getTranche_age()+"'"
														+ ");"   );
		}catch (Exception e){e.printStackTrace();}
		
	}
	
	public void inscrirUtilisateur(Utilisateur u){
		try{
			s.executeUpdate("INSERT INTO utilisateur (nom,prenom,cin,email,telephone,adresse,username,password,image,roles,description, username_canonical, email_canonical, enabled, salt, locked, expired, credentials_expired,grade,tranche_age,id_club) VALUES ('"
														+u.getNom()+"','"
														+u.getPrenom()+"','"
														+u.getCin()+"','"
														+u.getEmail()+"','"
														+u.getTelephone()+"','"
														+u.getAdresse()+"','"
														+u.getUsername()+"','"
														+u.getPassword()+"','"
														+u.getImage()+"','"
														+u.getRoles()+"','"														
														+u.getDescription()+"','"
														
														+u.getUsername_canonical()+"','"
														+u.getEmail_canonical()+"','"
														+u.getEnabled()+"','"
														+u.getSalt()+"','"
														+u.getLocked()+"','"
														+u.getExpired()+"','"
														+u.getCredentials_expired()+"','"
														
														
														
														
														+u.getGrade()+"','"
														+u.getTranche_age()+"','"
														
														+u.getId_club()+"'"
														
														
														+ ");"   );
		}catch (Exception e){e.printStackTrace();}
		
	}
	///////////
	

	//////////
	public List<Utilisateur>getUsers() { // users sans grade,id_clun,tanche_age
		List<Utilisateur> l = new ArrayList<Utilisateur>();
		try{
			r=s.executeQuery(" select * from utilisateur ");
			while(r.next()){
				Utilisateur u1 = new Utilisateur();
				
				u1.setId(r.getInt("id"));
				u1.setNom(r.getString("nom"));
				u1.setPrenom(r.getString("prenom"));
				u1.setCin(r.getInt("cin"));
				u1.setAdresse(r.getString("adresse"));
				u1.setTelephone(r.getInt("telephone"));
				
				u1.setGrade(r.getString("grade"));
				u1.setEmail(r.getString("email"));
				u1.setRoles(r.getString("roles"));
				
				u1.setUsername(r.getString("username"));
				u1.setPassword(r.getString("password"));
				u1.setDescription(r.getString("description"));
				
				
				
				
				l.add(u1);
			}
				return l;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	/////// fronction joueur
	
	public void inscrirJoueur(Utilisateur u){
		try{
			s.executeUpdate("INSERT INTO utilisateur (nom,prenom,cin,email,telephone,adresse,username,password,image,roles,description, username_canonical, email_canonical, enabled, salt, locked, expired, credentials_expired,grade,tranche_age,id_club) VALUES ('"
														+u.getNom()+"','"
														+u.getPrenom()+"','"
														+u.getCin()+"','"
														+u.getEmail()+"','"
														+u.getTelephone()+"','"
														+u.getAdresse()+"','"
														+u.getUsername()+"','"
														+u.getPassword()+"','"
														+u.getImage()+"','"
														+u.getRoles()+"','"														
														+u.getDescription()+"','"
														
														+u.getUsername_canonical()+"','"
														+u.getEmail_canonical()+"','"
														+u.getEnabled()+"','"
														+u.getSalt()+"','"
														+u.getLocked()+"','"
														+u.getExpired()+"','"
														+u.getCredentials_expired()+"','"
														
														
														
														
														+u.getGrade()+"','"
														+u.getTranche_age()+"','"
														
														+u.getId_club()+"'"
														
														
														+ ");"   );
		}catch (Exception e){e.printStackTrace();}
		
	}
	
	public List<Utilisateur>getGamers() {
		List<Utilisateur> l2 = new ArrayList<Utilisateur>();
		try{
			r=s.executeQuery(" select * from utilisateur where roles='Joueur'");
			while(r.next()){
				Utilisateur u1 = new Utilisateur();
				
				u1.setId(r.getInt("id"));
				u1.setNom(r.getString("nom"));
				u1.setPrenom(r.getString("prenom"));
				u1.setCin(r.getInt("cin"));
				u1.setAdresse(r.getString("adresse"));
				u1.setTelephone(r.getInt("telephone"));
				
				u1.setGrade(r.getString("grade"));
				u1.setEmail(r.getString("email"));
				u1.setRoles(r.getString("roles"));
				
				u1.setUsername(r.getString("username"));
				u1.setPassword(r.getString("password"));
				u1.setDescription(r.getString("description"));
				u1.setImage(r.getString("image"));
				u1.setTranche_age(r.getString("tranche_age"));
				u1.setId_club(r.getInt("id_club"));
				
				
				l2.add(u1);
			}
				return l2;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	//////
	
	public List<Utilisateur>getGamerByIdClub(int id_club) {
		List<Utilisateur> l2 = new ArrayList<Utilisateur>();
		try{
			r=s.executeQuery(" select * from utilisateur where id_club='"+id_club+"'and roles='Joueur';");
			while(r.next()){
				Utilisateur u1 = new Utilisateur();
				
				u1.setId(r.getInt("id"));
				u1.setNom(r.getString("nom"));
				u1.setPrenom(r.getString("prenom"));
				u1.setCin(r.getInt("cin"));
				u1.setAdresse(r.getString("adresse"));
				u1.setTelephone(r.getInt("telephone"));
				
				u1.setGrade(r.getString("grade"));
				u1.setEmail(r.getString("email"));
				u1.setRoles(r.getString("roles"));
				
				u1.setUsername(r.getString("username"));
				u1.setPassword(r.getString("password"));
				u1.setDescription(r.getString("description"));
				u1.setImage(r.getString("image"));
				u1.setTranche_age(r.getString("tranche_age"));
				u1.setId_club(r.getInt("id_club"));
				
				
				l2.add(u1);
			}
				return l2;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	
	//////
	public void deleteGamer(Utilisateur u){
		try{
			s.executeUpdate("DELETE FROM utilisateur where id='"+u.getId()+"' and roles='Joueur' ");
		}catch (Exception e){e.printStackTrace();}
		
	}
	
	public void updateGamer(Utilisateur u){
		try{
			s.executeUpdate("update utilisateur set nom= '"+u.getNom()
										+"' ,prenom= '"+u.getPrenom()
										+"' ,cin='"+u.getCin()
										+"' ,adresse='"+u.getAdresse()
										+"' ,telephone= '"+u.getTelephone()
										+"' ,grade= '"+u.getGrade()
										+"' ,email= '"+u.getEmail()
										+"' ,roles= '"+u.getRoles()
										+"' where id= '"+u.getId()+"'    ");
													
		}catch (Exception e){e.printStackTrace();}
		
	}
	
	public Utilisateur getGamerById(int id){
		Utilisateur u1 = new Utilisateur();
		try{
			r=s.executeQuery(" select * from utilisateur where id ='"+id+"' and roles='Joueur'");
			while(r.next()){
				
				
				u1.setId(r.getInt("id"));
				u1.setNom(r.getString("nom"));
				u1.setPrenom(r.getString("prenom"));
				u1.setCin(r.getInt("cin"));
				u1.setAdresse(r.getString("adresse"));
				u1.setTelephone(r.getInt("telephone"));
				
				u1.setGrade(r.getString("grade"));
				u1.setEmail(r.getString("email"));
				u1.setRoles(r.getString("roles"));
				
				u1.setUsername(r.getString("username"));
				u1.setPassword(r.getString("password"));
				u1.setDescription(r.getString("description"));
				u1.setImage(r.getString("image"));
				u1.setTranche_age(r.getString("tranche_age"));
				u1.setId_club(r.getInt("id_club"));
				
				
				
			}
				return u1;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	
	/////////////
	
	
	public List<Utilisateur>getGamersByGrade(String grade) {
		List<Utilisateur> l = new ArrayList<Utilisateur>();
		try{
			r=s.executeQuery(" select * from utilisateur where roles='Joueur' and grade='"+grade+"';");
			while(r.next()){
				Utilisateur u1 = new Utilisateur();
				
				u1.setId(r.getInt("id"));
				u1.setNom(r.getString("nom"));
				u1.setPrenom(r.getString("prenom"));
				u1.setCin(r.getInt("cin"));
				u1.setAdresse(r.getString("adresse"));
				u1.setTelephone(r.getInt("telephone"));
				u1.setImage(r.getString("image"));
				u1.setGrade(r.getString("grade"));
				u1.setEmail(r.getString("email"));
				u1.setRoles(r.getString("roles"));
				
				u1.setUsername(r.getString("username"));
				u1.setPassword(r.getString("password"));
				u1.setDescription(r.getString("description"));
				
				
				
				l.add(u1);
			}
				return l;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	
	// fin fctn joueur
	
	
	
	////// fonction arbitre
	public List<Utilisateur>getArbitres() {
		List<Utilisateur> l = new ArrayList<Utilisateur>();
		try{
			r=s.executeQuery(" select * from utilisateur where roles='Arbitre'");
			while(r.next()){
				Utilisateur u1 = new Utilisateur();
				
				u1.setId(r.getInt("id"));
				u1.setNom(r.getString("nom"));
				u1.setPrenom(r.getString("prenom"));
				u1.setCin(r.getInt("cin"));
				u1.setAdresse(r.getString("adresse"));
				u1.setTelephone(r.getInt("telephone"));
				u1.setImage(r.getString("image"));
				u1.setGrade(r.getString("grade"));
				u1.setEmail(r.getString("email"));
				u1.setRoles(r.getString("roles"));
				
				u1.setUsername(r.getString("username"));
				u1.setPassword(r.getString("password"));
				u1.setDescription(r.getString("description"));
				
				
				
				l.add(u1);
			}
				return l;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	
	public void deleteArbitre(Utilisateur u){
		try{
			s.executeUpdate("DELETE FROM utilisateur where id='"+u.getId()+"' and roles='Arbitre' ");
		}catch (Exception e){e.printStackTrace();}
		
	}
	
	public void updateArbitre(Utilisateur u){
		try{
			s.executeUpdate("update utilisateur set nom= '"+u.getNom()
										+"' ,prenom= '"+u.getPrenom()
										+"' ,cin='"+u.getCin()
										+"' ,adresse='"+u.getAdresse()
										+"' ,telephone= '"+u.getTelephone()
										+"' ,grade= '"+u.getGrade()
										+"' ,email= '"+u.getEmail()
										+"' ,roles= '"+u.getRoles()
										+"' where id= '"+u.getId()+"'    ");
													
		}catch (Exception e){e.printStackTrace();}
		
	}
	
	public void inscrirArbitre(Utilisateur u){
		try{
			s.executeUpdate("INSERT INTO utilisateur (nom,prenom,cin,email,telephone,adresse,username,password,image,roles,description, username_canonical, email_canonical, enabled, salt, locked, expired, credentials_expired,grade) VALUES ('"
														+u.getNom()+"','"
														+u.getPrenom()+"','"
														+u.getCin()+"','"
														+u.getEmail()+"','"
														+u.getTelephone()+"','"
														+u.getAdresse()+"','"
														+u.getUsername()+"','"
														+u.getPassword()+"','"
														+u.getImage()+"','"
														+u.getRoles()+"','"														
														+u.getDescription()+"','"
														
														+u.getUsername_canonical()+"','"
														+u.getEmail_canonical()+"','"
														+u.getEnabled()+"','"
														+u.getSalt()+"','"
														+u.getLocked()+"','"
														+u.getExpired()+"','"
														
														+u.getCredentials_expired()+"','"													
														+u.getGrade()+"'"
														
														
														+ ");"   );
		}catch (Exception e){e.printStackTrace();}
		
	}
	
	
	public Utilisateur getArbitreById(int id){
		Utilisateur u1 = new Utilisateur();
		try{
			r=s.executeQuery(" select * from utilisateur where id ='"+id+"' and roles='Arbitre'");
			while(r.next()){
				
				
				u1.setId(r.getInt("id"));
				u1.setNom(r.getString("nom"));
				u1.setPrenom(r.getString("prenom"));
				u1.setCin(r.getInt("cin"));
				u1.setAdresse(r.getString("adresse"));
				u1.setTelephone(r.getInt("telephone"));
				
				u1.setGrade(r.getString("grade"));
				u1.setEmail(r.getString("email"));
				u1.setRoles(r.getString("roles"));
				
				u1.setUsername(r.getString("username"));
				u1.setPassword(r.getString("password"));
				u1.setDescription(r.getString("description"));
				u1.setImage(r.getString("image"));
				u1.setTranche_age(r.getString("tranche_age"));
				u1.setId_club(r.getInt("id_club"));
				
				
				
			}
				return u1;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	/////
	public Utilisateur getMedecinById(int id){
		Utilisateur u1 = new Utilisateur();
		try{
			r=s.executeQuery(" select * from utilisateur where id ='"+id+"' and roles='Medecin'");
			while(r.next()){
				
				
				u1.setId(r.getInt("id"));
				u1.setNom(r.getString("nom"));
				u1.setPrenom(r.getString("prenom"));
				u1.setCin(r.getInt("cin"));
				u1.setAdresse(r.getString("adresse"));
				u1.setTelephone(r.getInt("telephone"));
				
				u1.setGrade(r.getString("grade"));
				u1.setEmail(r.getString("email"));
				u1.setRoles(r.getString("roles"));
				
				u1.setUsername(r.getString("username"));
				u1.setPassword(r.getString("password"));
				u1.setDescription(r.getString("description"));
				u1.setImage(r.getString("image"));
				u1.setTranche_age(r.getString("tranche_age"));
				u1.setId_club(r.getInt("id_club"));
				
				
				
			}
				return u1;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	////
	public List<Utilisateur>getArbitresByGrade(String grade) {
		List<Utilisateur> l = new ArrayList<Utilisateur>();
		try{
			r=s.executeQuery(" select * from utilisateur where roles='Arbitre' and grade='"+grade+"';");
			while(r.next()){
				Utilisateur u1 = new Utilisateur();
				
				u1.setId(r.getInt("id"));
				u1.setNom(r.getString("nom"));
				u1.setPrenom(r.getString("prenom"));
				u1.setCin(r.getInt("cin"));
				u1.setAdresse(r.getString("adresse"));
				u1.setTelephone(r.getInt("telephone"));
				u1.setImage(r.getString("image"));
				u1.setGrade(r.getString("grade"));
				u1.setEmail(r.getString("email"));
				u1.setRoles(r.getString("roles"));
				
				u1.setUsername(r.getString("username"));
				u1.setPassword(r.getString("password"));
				u1.setDescription(r.getString("description"));
				
				
				
				l.add(u1);
			}
				return l;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	
	
	///////fin fnct arbitre
	
	
	/// les fonctions des medecin
	public void ajouterMedecin(Utilisateur u){
		try{
			s.executeUpdate("INSERT INTO utilisateur (nom,prenom,cin,email,telephone,adresse,username,password,image,roles,description, username_canonical, email_canonical, enabled, salt, locked, expired, credentials_expired) VALUES ('"
														+u.getNom()+"','"
														+u.getPrenom()+"','"
														+u.getCin()+"','"
														+u.getEmail()+"','"
														+u.getTelephone()+"','"
														+u.getAdresse()+"','"
														+u.getUsername()+"','"
														+u.getPassword()+"','"
														+u.getImage()+"','"
														+u.getRoles()+"','"														
														+u.getDescription()+"','"
														
														+u.getUsername_canonical()+"','"
														+u.getEmail_canonical()+"','"
														+u.getEnabled()+"','"
														+u.getSalt()+"','"
														+u.getLocked()+"','"
														+u.getExpired()+"','"
																											
														
																										
														
														+u.getCredentials_expired()+"'"
														+ ");"   );
		}catch (Exception e){e.printStackTrace();}
		
	}
	
	public List<Utilisateur>getMedecins() {
		List<Utilisateur> lm = new ArrayList<Utilisateur>();
		try{
			r=s.executeQuery(" select * from utilisateur where roles='Medecin'");
			while(r.next()){
				Utilisateur u = new Utilisateur();
				
				u.setId(r.getInt("id"));
				u.setNom(r.getString("nom"));
				u.setPrenom(r.getString("prenom"));
				u.setCin(r.getInt("cin"));
				u.setAdresse(r.getString("adresse"));
				u.setTelephone(r.getInt("telephone"));
				
				u.setGrade(r.getString("grade"));
				u.setEmail(r.getString("email"));
				u.setRoles(r.getString("roles"));
				
				u.setImage(r.getString("image"));
				
				lm.add(u);
			}
				return lm;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	public void deleteMedecin(Utilisateur u){
		try{
			s.executeUpdate("DELETE FROM utilisateur where id='"+u.getId()+"' and roles='Medecin' ");
		}catch (Exception e){e.printStackTrace();}
		
	}
	
	public void updateMedecin(Utilisateur u){
		try{
			s.executeUpdate("update utilisateur set nom= '"+u.getNom()
										+"' ,prenom= '"+u.getPrenom()
										+"' ,cin='"+u.getCin()
										+"' ,adresse='"+u.getAdresse()
										+"' ,telephone= '"+u.getTelephone()
										+"' ,grade= '"+u.getGrade()
										+"' ,email= '"+u.getEmail()
										+"' ,roles= '"+u.getRoles()
										+"' where id= '"+u.getId()+"'    ");
													
		}catch (Exception e){e.printStackTrace();}
		
	}
	
	// fin fcnt medecin
	
	/// les fonctions des visiteur
	public void InscrireVisiteur(Utilisateur u){
		try{
			s.executeUpdate("INSERT INTO utilisateur (nom,prenom,cin,email,telephone,adresse,username,password,image,roles,description, username_canonical, email_canonical, enabled, salt, locked, expired, credentials_expired) VALUES ('"
														+u.getNom()+"','"
														+u.getPrenom()+"','"
														+u.getCin()+"','"
														+u.getEmail()+"','"
														+u.getTelephone()+"','"
														+u.getAdresse()+"','"
														+u.getUsername()+"','"
														+u.getPassword()+"','"
														+u.getImage()+"','"
														+u.getRoles()+"','"														
														+u.getDescription()+"','"
														
														+u.getUsername_canonical()+"','"
														+u.getEmail_canonical()+"','"
														+u.getEnabled()+"','"
														+u.getSalt()+"','"
														+u.getLocked()+"','"
														+u.getExpired()+"','"
																											
														
																										
														
														+u.getCredentials_expired()+"'"
														+ ");"   );
		}catch (Exception e){e.printStackTrace();}
		
	}
	
	public List<Utilisateur>getVisiteurs() {
		List<Utilisateur> lm = new ArrayList<Utilisateur>();
		try{
			r=s.executeQuery(" select * from utilisateur where roles='Visiteur'");
			while(r.next()){
				Utilisateur u = new Utilisateur();
				
				u.setId(r.getInt("id"));
				u.setNom(r.getString("nom"));
				u.setPrenom(r.getString("prenom"));
				u.setCin(r.getInt("cin"));
				u.setAdresse(r.getString("adresse"));
				u.setTelephone(r.getInt("telephone"));
				
				u.setGrade(r.getString("grade"));
				u.setEmail(r.getString("email"));
				u.setRoles(r.getString("roles"));
				
				u.setImage(r.getString("image"));
				
				lm.add(u);
			}
				return lm;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	public void deleteVisiteur(Utilisateur u){
		try{
			s.executeUpdate("DELETE FROM utilisateur where id='"+u.getId()+"' and roles='Visiteur' ");
		}catch (Exception e){e.printStackTrace();}
		
	}
	
	public void updateVisiteur(Utilisateur u){
		try{
			s.executeUpdate("update utilisateur set nom= '"+u.getNom()
										+"' ,prenom= '"+u.getPrenom()
										+"' ,cin='"+u.getCin()
										+"' ,adresse='"+u.getAdresse()
										+"' ,telephone= '"+u.getTelephone()
										+"' ,grade= '"+u.getGrade()
										+"' ,email= '"+u.getEmail()
										+"' ,roles= '"+u.getRoles()
										+"' where id= '"+u.getId()+"'    ");
													
		}catch (Exception e){e.printStackTrace();}
		
	}
	
	// fin fcnt visiteur
	
	///////// les getBn
	public int getNbAdmin(){
		try{
			r=s.executeQuery(" SELECT COUNT(*) AS total FROM utilisateur where roles='Administrateur'");
			r.next();
			return r.getInt("total");
		}catch(Exception e){e.printStackTrace();return 0;}	
	}
	public int getNbArbitre(){
		try{
			r=s.executeQuery(" SELECT COUNT(*) AS total FROM utilisateur where roles='Arbitre'");
			r.next();
			return r.getInt("total");
		}catch(Exception e){e.printStackTrace();return 0;}	
	}
	public int getNbJoueur(){
		try{
			r=s.executeQuery(" SELECT COUNT(*) AS total FROM utilisateur where roles='Joueur'");
			r.next();
			return r.getInt("total");
		}catch(Exception e){e.printStackTrace();return 0;}	
	}
	public int getNbMedecin(){
		try{
			r=s.executeQuery(" SELECT COUNT(*) AS total FROM utilisateur where roles='Medecin'");
			r.next();
			return r.getInt("total");
		}catch(Exception e){e.printStackTrace();return 0;}	
	}
	public int getNbRespensable(){
		try{
			r=s.executeQuery(" SELECT COUNT(*) AS total FROM utilisateur where roles='Respensable'");
			r.next();
			return r.getInt("total");
		}catch(Exception e){e.printStackTrace();return 0;}	
	}
	public int getNbVisiteur(){
		try{
			r=s.executeQuery(" SELECT COUNT(*) AS total FROM utilisateur where roles='Visiteur'");
			r.next();
			return r.getInt("total");
		}catch(Exception e){e.printStackTrace();return 0;}	
	}
	////
	
	public int getNbJoueurByClubId(int id){
		try{
			r=s.executeQuery(" SELECT COUNT(*) AS total FROM utilisateur where roles='Joueur' and id_club='"+id+"';");
			r.next();
			return r.getInt("total");
		}catch(Exception e){e.printStackTrace();return 0;}	
	}
}
