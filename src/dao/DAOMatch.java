package dao;


import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import entities.Competition;
import entities.Evenement;
import entities.Match;

public class DAOMatch {
	
	
	Statement s;
	ResultSet r;
	List<Match> listMatch=new ArrayList<Match>();
	
	public DAOMatch() {
		try{
			s=Connect.getInstance().createStatement();
		}catch (Exception e) {e.printStackTrace();}
	}
	
	SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat sdf_date = new java.text.SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat sdf_time = new java.text.SimpleDateFormat("HH:mm:ss");
	
	public void ajouterMatch(Match u){
		try{
			s.executeUpdate("INSERT INTO pidev2.match (lieux,type,date_debut,date_fin) VALUES ('"
														+u.getLieux()+"','"
														+u.getType()+"','"
														+sdf.format(u.getDate_debut())+"','"												
														+sdf.format(u.getDate_fin())+"'"
														+ ");"   );
		}catch (Exception e){e.printStackTrace();}
		
	}
	
	
	
	public List<Match>getMatchs() {
		List<Match> lm = new ArrayList<Match>();
		try{
			r=s.executeQuery("select * from pidev2.match");
			while(r.next()){
				Match u = new Match();
				
				u.setId(r.getInt("id"));
				u.setLieux(r.getString("lieux"));
				u.setType(r.getString("type"));
				
				
				/////
				String st_date_debut = sdf_date.format(r.getDate("date_debut"));
				String st_time_debut = sdf_time.format(r.getTime("date_debut"));
				Date date_debut = sdf.parse(st_date_debut+" "+st_time_debut);
				u.setDate_debut(date_debut);
				//
				String st_date_fin = sdf_date.format(r.getDate("date_fin"));
				String st_time_fin = sdf_time.format(r.getTime("date_fin"));
				Date date_fin = sdf.parse(st_date_fin+" "+st_time_fin);
				u.setDate_fin(date_fin);
				//////
				
				
				u.setId_competition(r.getInt("id_competition"));
				u.setId_event(r.getInt("id_event"));

				lm.add(u);
			}
				return lm;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	/////
	
	public List<Match>getMatchsByCompetitionId(int id) {
		List<Match> lm = new ArrayList<Match>();
		try{
			r=s.executeQuery("select * from pidev2.match where id_competition='"+id+"';");
			while(r.next()){
				Match u = new Match();
				
				u.setId(r.getInt("id"));
				u.setLieux(r.getString("lieux"));
				u.setType(r.getString("type"));
				
			/////
							String st_date_debut = sdf_date.format(r.getDate("date_debut"));
							String st_time_debut = sdf_time.format(r.getTime("date_debut"));
							Date date_debut = sdf.parse(st_date_debut+" "+st_time_debut);
							u.setDate_debut(date_debut);
							//
							String st_date_fin = sdf_date.format(r.getDate("date_fin"));
							String st_time_fin = sdf_time.format(r.getTime("date_fin"));
							Date date_fin = sdf.parse(st_date_fin+" "+st_time_fin);
							u.setDate_fin(date_fin);
							//////
							
				System.out.println("get match "+r.getDate("date_debut"));
				
				u.setId_competition(r.getInt("id_competition"));
				u.setId_event(r.getInt("id_event"));

				lm.add(u);
			}
				return lm;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	
	////
	
	
	public List<Match>getMatchsByEventId(int id) {
		List<Match> lm = new ArrayList<Match>();
		try{
			r=s.executeQuery("select * from pidev2.match where id_event='"+id+"';");
			while(r.next()){
				Match u = new Match();
				
				u.setId(r.getInt("id"));
				u.setLieux(r.getString("lieux"));
				u.setType(r.getString("type"));
				
			/////
							String st_date_debut = sdf_date.format(r.getDate("date_debut"));
							String st_time_debut = sdf_time.format(r.getTime("date_debut"));
							Date date_debut = sdf.parse(st_date_debut+" "+st_time_debut);
							u.setDate_debut(date_debut);
							//
							String st_date_fin = sdf_date.format(r.getDate("date_fin"));
							String st_time_fin = sdf_time.format(r.getTime("date_fin"));
							Date date_fin = sdf.parse(st_date_fin+" "+st_time_fin);
							u.setDate_fin(date_fin);
							//////
							
				
				System.out.println("get match "+r.getDate("date_debut"));
				
				u.setId_competition(r.getInt("id_competition"));
				u.setId_event(r.getInt("id_event"));

				lm.add(u);
			}
				return lm;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	
	////
	
	public Match getMatchById(int id) {
		Match u = new Match();
		try{
			r=s.executeQuery("select * from pidev2.match where id='"+id+"';");
			while(r.next()){
				
				
				u.setId(r.getInt("id"));
				u.setLieux(r.getString("lieux"));
				u.setType(r.getString("type"));
				
			/////
							String st_date_debut = sdf_date.format(r.getDate("date_debut"));
							String st_time_debut = sdf_time.format(r.getTime("date_debut"));
							Date date_debut = sdf.parse(st_date_debut+" "+st_time_debut);
							u.setDate_debut(date_debut);
							//
							String st_date_fin = sdf_date.format(r.getDate("date_fin"));
							String st_time_fin = sdf_time.format(r.getTime("date_fin"));
							Date date_fin = sdf.parse(st_date_fin+" "+st_time_fin);
							u.setDate_fin(date_fin);
							//////
							
				
				System.out.println("get match "+r.getDate("date_debut"));
				
				u.setId_competition(r.getInt("id_competition"));
				u.setId_event(r.getInt("id_event"));

				
			}
				return u;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	
	/////
	public List<Match>getMatchsByLieuxId(int id) {
		List<Match> lm = new ArrayList<Match>();
		try{
			r=s.executeQuery("select * from pidev2.match where lieux='"+id+"';");
			while(r.next()){
				Match u = new Match();
				
				u.setId(r.getInt("id"));
				u.setLieux(r.getString("lieux"));
				u.setType(r.getString("type"));
				
			/////
							String st_date_debut = sdf_date.format(r.getDate("date_debut"));
							String st_time_debut = sdf_time.format(r.getTime("date_debut"));
							Date date_debut = sdf.parse(st_date_debut+" "+st_time_debut);
							u.setDate_debut(date_debut);
							//
							String st_date_fin = sdf_date.format(r.getDate("date_fin"));
							String st_time_fin = sdf_time.format(r.getTime("date_fin"));
							Date date_fin = sdf.parse(st_date_fin+" "+st_time_fin);
							u.setDate_fin(date_fin);
							//////
							
				System.out.println("get match "+r.getDate("date_debut"));
				
				u.setId_competition(r.getInt("id_competition"));
				u.setId_event(r.getInt("id_event"));

				lm.add(u);
			}
				return lm;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	
	////
	
	
	////
	
	public void deleteMatch(Match u){
		try{
			s.executeUpdate("DELETE FROM match_user where id_match='"+u.getId()+"' ");
			s.executeUpdate("DELETE FROM pidev2.match where id='"+u.getId()+"' ");
		}catch (Exception e){e.printStackTrace();}
	}
	
	
	
	public void updateMatch(Match u){
		try{
			
			System.out.println("dao update "+sdf.format(u.getDate_debut()));
			System.out.println("dao update "+sdf.format(u.getDate_fin()));
			
			s.executeUpdate("update pidev2.match set lieux= '"+u.getLieux()
										+"' ,type= '"+u.getType()
										+"' ,date_debut='"+sdf.format(u.getDate_debut())
										+"' ,date_fin='"+sdf.format(u.getDate_fin())
										
										+"' where id= '"+u.getId()+"'    ");				
		}catch (Exception e){e.printStackTrace();}
	}
	////
	public void affectMatchEvent(Match m, Evenement event){
		try{
			
			s.executeUpdate("update pidev2.match set id_event= '"+event.getId()
										+"' where id= '"+m.getId()+"'    ");				
		}catch (Exception e){e.printStackTrace();}
	}
	////
	public void affectMatchComp(Match m, Competition comp){
		try{
			
			s.executeUpdate("update pidev2.match set id_competition= '"+comp.getId()
										+"' where id= '"+m.getId()+"'    ");				
		}catch (Exception e){e.printStackTrace();}
	}
	
	
}
