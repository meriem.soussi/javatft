package dao;

import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import entities.Competition;
import entities.Match;

public class DAOCompetition {
	

	Statement s;
	ResultSet r;
		
	public DAOCompetition() {
		try{
			s=Connect.getInstance().createStatement();
		}catch (Exception e) {e.printStackTrace();}
	}
	
	SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	SimpleDateFormat sdf_date = new java.text.SimpleDateFormat("yyyy-MM-dd");
	SimpleDateFormat sdf_time = new java.text.SimpleDateFormat("HH:mm:ss");
	public void ajouterCompetition(Competition u){
		try{
			s.executeUpdate("INSERT INTO competition (lieux,nom,date_debut,type,date_fin) VALUES ('"
														+u.getLieux()+"','"
														+u.getNom()+"','"
														+sdf.format(u.getDate_debut())+"','"
														+u.getType()+"','"
														+sdf.format(u.getDate_fin())+"'"
														+ ");"   );
		}catch (Exception e){e.printStackTrace();}
		
	}
	
	
	
	public List<Competition>getCompetitions() {
		List<Competition> lm = new ArrayList<Competition>();
		try{
			r=s.executeQuery(" select * from competition");
			while(r.next()){
				Competition u = new Competition();
				
				u.setId(r.getInt("id"));
				u.setLieux(r.getString("lieux"));
				u.setNom(r.getString("nom"));
				u.setType(r.getString("type"));
				/////
				String st_date_debut = sdf_date.format(r.getDate("date_debut"));
				String st_time_debut = sdf_time.format(r.getTime("date_debut"));
				Date date_debut = sdf.parse(st_date_debut+" "+st_time_debut);
				u.setDate_debut(date_debut);
				//
				String st_date_fin = sdf_date.format(r.getDate("date_fin"));
				String st_time_fin = sdf_time.format(r.getTime("date_fin"));
				Date date_fin = sdf.parse(st_date_fin+" "+st_time_fin);
				u.setDate_fin(date_fin);
				//////
				

				lm.add(u);
			}
				return lm;
			}catch(Exception e){e.printStackTrace();return null;}
	}
	
	
	
	
	
	public void deleteCompetition(Competition u, List<Match> listMatch){
		try{
			int t = listMatch.size();
			for(int i=0;i<t;i++){
				s.executeUpdate("DELETE FROM match_user where id='"+listMatch.get(i).getId()+"' ");
			}
			
			
			
			
			s.executeUpdate("DELETE FROM pidev2.match where id_competition="+u.getId()+" ");
			s.executeUpdate("DELETE FROM comp_user where id_competition='"+u.getId()+"' ");
			s.executeUpdate("DELETE FROM competition where id='"+u.getId()+"' ;");
		}catch (Exception e){e.printStackTrace();}
	}
	
	
	
	public void updateCompetition(Competition u){
		try{
			s.executeUpdate("update competition set lieux= '"+u.getLieux()
										+"' ,nom= '"+u.getNom()
										+"' ,date_debut='"+sdf.format(u.getDate_debut())
										+"' ,date_fin='"+sdf.format(u.getDate_fin())
										+"' ,type='"+u.getType()
										+"' where id= '"+u.getId()+"'    ");				
		}catch (Exception e){e.printStackTrace();}
	}
	
	public Competition getCompById(int id){
		Competition u = new Competition();
		try{
			r=s.executeQuery(" select * from competition where id="+id);
			while(r.next()){
				
				
				u.setId(r.getInt("id"));
				u.setLieux(r.getString("lieux"));
				u.setNom(r.getString("nom"));
				
				/////
				String st_date_debut = sdf_date.format(r.getDate("date_debut"));
				String st_time_debut = sdf_time.format(r.getTime("date_debut"));
				Date date_debut = sdf.parse(st_date_debut+" "+st_time_debut);
				u.setDate_debut(date_debut);
				//
				String st_date_fin = sdf_date.format(r.getDate("date_fin"));
				String st_time_fin = sdf_time.format(r.getTime("date_fin"));
				Date date_fin = sdf.parse(st_date_fin+" "+st_time_fin);
				u.setDate_fin(date_fin);
				//////
				
				

				
			}
				return u;
			}catch(Exception e){e.printStackTrace();return null;}
	}
}
